plot_fire_in_xx_space_cfg <- function() {
	source("libs/make.transparent.r")
	source("libs/dev.off.all.r")

	alpha_input		<<- "data/Prob_fire_alpha.csv"
	MAP_input		<<- "data/Prob_fire_MAP.csv"
	NPP_input		<<- "data/Prob_fire_NPP.csv"
	sav_input		<<- "data/Prob_sav_MAP.csv"
	pop_input		<<- "data/Prop_fire_pop.csv"
	popr_input		<<- "data/Prob_fire_prop_res.csv"
	
	fire_col		<<- 'red'
	sav_col			<<- 'yellow'
	NPP_col			<<- 'green'
	alpha_col		<<- 'blue'
	pop_col			<<- 'black'
	
	dev.off.all()
	png("figs/unimodel.png",height=7,width=6,unit='in',res=600)
	par(mfrow=c(2,1),oma=c(0,0,0,0),mar=c(3,4,1,1))
	
}

plot_fire_in_xx_space	<- function() {
	
	plot_fire_in_xx_space_cfg()
	
	alpha	=read.csv(alpha_input)
	MAP		=read.csv(MAP_input)
	NPP		=read.csv(NPP_input)
	sav		=read.csv(sav_input)
	pop		=read.csv(pop_input)
	popr	=read.csv(popr_input)
	
	
	pop		=read.csv(pop_input)
	
	
	MAP[,2]=MAP[,2]*100
	sav[,2]=sav[,2]*100
	
	plot_relationship(MAP,sav,NPP,alpha,
					'MAP','% Burnt area / Savannah Cover',
					c(fire_col,sav_col,NPP_col,alpha_col))	
					
	add_arrows('NPP','Dryness')		
	add_legend(c('Fire','Savannah','NPP',expression(alpha)),
			   c(fire_col,sav_col,make.transparent(c(NPP_col,alpha_col),0.99)))
	
	
					
	plot_relationship(pop,NULL,NPP,popr,
					expression(paste('Population Density (people/km'^{2},')',sep="")),
					'% Burnt area',
					c(fire_col,sav_col,NPP_col,pop_col),p1=0.5,p2=3/10,xaxt='n')
	
	locs=c(0.1,.3,1,3,10,30)
	axis(side=1,at=log(locs),labels=locs)
	
	add_arrows('NPP','Human Suppression   ')	
	
	add_legend(c('Fire','NPP','Pop. density'),
			   c(fire_col,make.transparent(c(NPP_col,pop_col),0.99)))
	
	dev.off()
}



add_legend <- function(leg_txt,col) {

	legendd <- function(lwd,leg_txt,col) {
		if (length(leg_txt)==4) {
			lwd=c(4,4,lwd,lwd)
			lty=c(1,3,1,1)
		} else {
			
			lwd=c(4,lwd,lwd)
			lty=c(1,1,1)
		}
		
		legend(x='topleft',legend=leg_txt,col=col,lwd=lwd,lty=lty,
			bty='n')
	}
	
	
	lapply((1:50)/2.5,legendd,leg_txt,col)
}


add_arrows <- function(lab1,lab2) {
	find_range <- function(i) {
		r=c(par('usr')[i])
		r[3]=r[2]-r[1]
		return(r)
	}
	
	xrange=find_range(1:2)
	yrange=find_range(3:4)
	y=yrange[1]+yrange[2]*0.07
	
	arrows(x0=xrange[1]+xrange[3]/4,y0=y,
	       x1=xrange[1]+0.45*xrange[3],y1=y,lwd=5,length=0.1,xpd=TRUE)
	       
	text(x=xrange[1]+0.35*xrange[3],y=2*y,lab1,xpd=TRUE)
	
	arrows(x0=xrange[1]+0.55*xrange[3],y0=y,
		   x1=xrange[2]-xrange[3]/4,y1=y,lwd=5,length=0.1,xpd=TRUE,code=1)
	text(x=xrange[1]+0.65*xrange[3],y=2*y,lab2,xpd=TRUE)
}


plot_relationship 	<- function(main1,main2=NULL,poly1,poly2,
								xlab,ylab,col=c('red','yellow','green','blue'),
								p1=3/4,p2=1/2,...) {
								
	maxy=max(c(main1[,2],main2[,2]))
	plot(c(min(main1[,1]),max(main1[,1])),c(0,maxy),type='n',xlab='',ylab='',
		xaxs='i',yaxs='i',...)
	
		max_main1=max(main1)
	min_main1=min(main1)
	mtext(xlab,side=1,line=2)
	mtext(ylab,side=2,line=2)
	
	plot_polygons(poly1[,1],poly1[,2],min_main1,p1*max_main1,maxy,col[3])
	plot_polygons(poly2[,1],poly2[,2],min_main1+p2*max_main1,max_main1,maxy,col[4])
	
	lines(main1[,1],main1[,2],col=col[1],lwd=4)
	if (!is.null(main2)) lines(main2[,1],main2[,2],col=col[2],lwd=4,lty=4)
}



plot_polygon <-function(sc,x,y,minx=0,maxx,maxy,col,trans=0.998,shimmer=TRUE) {

		x=c(x,rev(x))
		x=x*(maxx-minx)/max(x)+minx
		if (shimmer) bx=x*(10+sample((-100:100)/100,1))/(10)
		
		polygon(x,
			1.3*maxy*c(y*(1-sc),rev(y)*(1+sc))/max(y),
			col=make.transparent(col,trans),border='transparent')
}

plot_polygons <-function(x,y,minx,maxx,maxy,col) {
	lapply((0:100)/400,plot_polygon,x,y,minx,maxx,maxy,col)
	plot_polygon(0.25,x,y,minx,maxx,maxy,col,trans=0.8,shimmer=FALSE)
}