plot_austalian_fire_cfg <- function() {
	source("libs/plot_raster_from_raster.r")
	source("libs/make_col_vector.r")
	source("libs/dev.off.all.r")
	source("libs/return_multiple_from_functions.r")
	source("libs/find_seasonal_concentration_and_phase.r")
	source("libs/raster_funs.r")
	source("libs/quantile.raster90.r")
	library(raster)
	dev.off.all()
	filename_fire		<<- 	'data/Fire_GFEDv4_Burnt_fraction_0.5grid-nc3.nc'
	#filename_fire		<<- 	'data/fire_GFED.nc'
	varname_fire		<<- 	'mfire_frac'
	
	xlims				<<- 	c(-180,180)
	ylims				<<-		c(-60,90)
	
	limits_bf			<<- 	c(0,0.001,0.01,0.02,0.05,0.1,.2,0.3)*100
	limits_seasonal		<<- 	1.5:11.5
	limits_cons			<<-		c(0,0.25,0.5,0.7,0.8,0.9)
	
	cols_bf				<<- 	c("white","#EE6600","#AA0000","#220000")
	cols_yr_timing		<<- 	c("#EE5500","yellow","blue","green","red")
	#cols_yr_timing		<<- 	c("blue","cyan","red","yellow",'blue')
	cols_cons			<<-		c("white","#FF77AA","#FF3344","#CC0000","#551200","#140200")
	#cols_cons				<<- 	c("white","#EE0066","#AA0000","#220000")
	leg_loc				<<-		c(0.1,0.6,0.07,0.10)
	
	slim				<<-		TRUE
	
	extentt				<<- extent(112.5,160,-44,-10.75)
}

plot_austalian_fire <- function() {
	plot_austalian_fire_cfg()
	if (slim) stepup_figure_slim() else stepup_figure()
	
	dat=brick(filename_fire,varname=varname_fire)
	dat=100*crop(dat,extentt)
	dat=dropLayer(dat,1:7)
	
	plot_bf(dat)
	plot_seasonal(dat)
	if (!slim) {
		plot_limitation()
		plot_veg_map()
	}
	dev.off()
	
}


stepup_figure <- function() {
	png("figs/australian_fires.png",height=7,width=7,res=600,unit='in')
	layoutM = t(matrix(c(1,1,1,2,2,2,
					     1,1,1,2,3,2,
					     1,1,1,2,2,2,
					     4,4,4,5,5,5,
					     6,6,6,7,7,7),ncol=5))
	layout(layoutM,heights=c(0.53,0.4,0.07,1,1),widths=c(0.19,0.35,0.46,0.25,0.3,0.45))
	par(mar=c(0,0,2,0),oma=c(0,0,0.3,0))
}

stepup_figure_slim <- function() {
	png("figs/australian_fires.png",height=3,width=10.5,res=600,unit='in')
	layoutM = t(matrix(c(1,1,1,2,2,2,4,4,4,
					     1,1,1,2,3,2,4,4,4,
					     1,1,1,2,2,2,4,4,4),ncol=3))
	layout(layoutM,heights=c(0.53,0.4,0.07),widths=c(0.19,0.35,0.46,0.20,0.35,0.45,0.49,0.49,0.02))
	par(mar=c(0,0,2,0),oma=c(0,0,0.3,0))
}



interp.basic <- function(x,y,z,length=40,...)
	interp(x, y, z, xo=seq(min(x), max(x), length = length),
       yo=seq(min(y), max(y), length = length),...)

plot_bf <- function(dat) {
	
	dat=mean(dat)*12
	plot_raster_from_raster(dat,limits=limits_bf,cols=cols_bf,add_legend=FALSE)
	
	if (!slim) contour(dat,levels=0.000001, drawlabels=FALSE,add=TRUE,col='#555500')
	aread=area(dat,na.rm=TRUE)

	print(paste("area with periodic fire:",100*(1-sum(aread[dat==0])/sum(values(aread),na.rm=TRUE))))
	mtext("    a)",adj=0,line=-2,cex=2)
	
	add_raster_legend2(make_col_vector(cols_bf,ncols=length(limits_bf)+1),limits_bf,
					   main_title=' ',xpd=TRUE,plot_loc=leg_loc)
	
	text(x=143.5,y=-43.8,"%")
}


plot_seasonal <- function(dat) {
	
	Cdat=layer.apply(1:12,function(i) mean(dat[[seq(i,nlayers(dat),12)]]))
	c(phase,cons):=find_seasonal_concentration_and_phase(Cdat)
	
	#plot_raster_from_raster(12*(360-phase)/360,limits=limits_seasonal,
	#		cols=make.transparent(cols_yr_timing,0.9),add_legend=FALSE,smooth_factor=1,shimmer=TRUE)
			
			
	phase0=phase
	limits_seasonal0=limits_seasonal
	cols_yr_timing0=cols_yr_timing
	phase=360-phase
	phase[phase<180]=phase[phase<180]+360
	
	phase[phase[1:3000]>450]=phase[phase[1:3000]>450]-360
	phase=phase*12/360
	limits_seasonal=c(limits_seasonal, limits_seasonal+12, limits_seasonal+24, limits_seasonal+36)
	cols_yr_timing=c(cols_yr_timing,rep(cols_yr_timing[-1],3))
	
	plot_raster_from_raster(phase,limits=limits_seasonal,cols=cols_yr_timing,add_legend=FALSE)
	quantile.raster90 (Cdat)
	mtext("    b)",adj=0,line=-2,cex=2)
	
	pie(rep(1,12),labels=c("Sum"," "," ","Aut "," "," ","Win"," "," "," Spr"," "," "),
		col=make_col_vector(cols_yr_timing0,ncols=12),xpd=TRUE)
	
	
	
	cons=quantile.raster90(Cdat)
	limits_cons=1:11
	Cdat0=Cdat
	plot_raster_from_raster(cons,limits=limits_cons,cols=cols_cons,add_legend=FALSE)
	cons=disaggregate(cons,method='bilinear',fact=5)
	mask=disaggregate(dat[[1]],method='bilinear',fact=5)
	#image(is.na(cons)+is.na(mask),col=c("transparent","grey","transparent"),add=TRUE)
	mtext("    c)",adj=0,line=-2,cex=2)
	leg_loc[2]=0.5
	add_raster_legend2(make_col_vector(cols_cons,ncols=length(limits_cons)+1),limits_cons,labelss=0:12,
					   xpd=TRUE,plot_loc=leg_loc)
					   
	text(x=140,y=-39,"months")
}

plot_limitation <- function() {
	plot.new()
	mtext("         d) Control limitation",adj=0,line=1)
}

plot_veg_map <- function() {

	plot.new()
	mtext("         e) Murphy et al. 2013",adj=0,line=1)

}