fire_construction_from_stats_model_setup <- function() {
	source("libs/return_multiple_from_functions.r")
	source("libs/layer.apply.r")
	return_multiple_from_functions("raster")
	source("libs/plot_3way.r")
	source("libs/plotting_intermediates.r")
	
	input 			<<- 'outputs/AusClim.csv'	
	filename_out 	<<- 'outputs/glm_test'
	clim_test 		<<- c("MAP","PC","IAVpr","MI",
                      		"fapar","faparC","NPP")
                      		
	clim_test 		<<- c("PC","IAVpr","MI","faparC","NPP")
	clim_test_glm 	<<- c("MI","NPP","lightn","PC","IAVpr","faparC","fapar","MAP")
	clim_test_nls	<<- c("DMI","NPP","Dlightn")
	clim_trans		<<- c("MI","fuel","lightn","None","None","None","None","lightn")
	clim_trans		<<- c("None","None","None","None","None","None","None","lightn")
	fire_obs 		<<- c("GFED3","GFED4","AVHRR")
	
	filename_out_glm<<- 'outputs/fire_glm'
	filename_out_nls<<- 'outputs/fire_nls'
	
	cols_3way		<<- rev(c("FF","DF","BF","9F","6F","3F"))	
	limits_3way		<<- c(0.33,0.5,0.67)
	smooth_image	<<- TRUE
	smooth_factor	<<- 5
		
}

	
fire_construction_from_stats_model <- function() {
	fire_construction_from_stats_model_setup()
	
	idata=read.csv(input)
	
	idata=do_transformation(idata)
	
	perform_and_write_stats <- function(pnls,clim_test,filename,...) {
		clim_test <<- clim_test
		otable=set_up_ouput_matrix(6,0,paste(rep(fire_obs,each=2),'.',rep(c("All","Sig"),3),sep=""),...)
	
		ii=c(-1,0)
		for (i in fire_obs) { ii=ii+2
			otable[,ii]=perform_residual_comparisons(i,idata,pnls=pnls,...)	
		}
	
		write_out_pvalues(otable,"All_products",filename)
	}
	
	perform_and_write_stats(FALSE,clim_test_glm,filename_out_glm)
	perform_and_write_stats(TRUE, clim_test_nls,filename_out_nls,rownames=c("a","b","c","d","e"))
}	

do_transformation <- function(idata) {
	do_trans <- function (i,idata,clim_test,clim_trans) {
		if (clim_trans[i]=="log") idata[[clim_test[i]]]=log(idata[[clim_test[i]]])
		
		if (clim_trans[i]=="MI")
			idata[[clim_test[i]]]=-1+2/(1+exp(-idata[[clim_test[i]]]/max(idata[[clim_test[i]]])	))
		
		if (clim_trans[i]=="fuel") idata[[clim_test[i]]]=-1+2/(1+exp(-idata[[clim_test[i]]]/max(idata[[clim_test[i]]])	))
		
		if (clim_trans[i]=="lightn") idata[[clim_test[i]]]=-1+2/(1+exp(-idata[[clim_test[i]]]/max(idata[[clim_test[i]]])	))
		
		return(idata)
	}
	
	for (i in 1:length(clim_test)) 
		idata=do_trans(i,idata,clim_test,clim_trans)
	
	return(idata)
	
}
		
perform_residual_comparisons <- function(varname,idata,pnls=FALSE,pglm=!pnls,...) {
	gdata=set_up_ouput_matrix(...)
	pdata=set_up_ouput_matrix(...)
	
	if (pglm) c(gdata,pdata):=perform_glm_comp_on_all(varname,idata,gdata,pdata)
	if (pnls) c(gdata,pdata):=perform_nls_comp_on_all(varname,idata,gdata,pdata)
	
	gdata=add_stars(gdata,pdata)
	
	return(gdata)
}


perform_glm_comp_on_all<- function(varname,idata,gdata,pdata,...) {
	all.formula=make_formula(varname,clim_test)
	
	glm_sum=summary(glm(all.formula,data=idata,family=quasibinomial(),...))$coefficients #
	
	gdata[,1]=glm_sum[,1]
	pdata[,1]=glm_sum[,4]
	
	if (glm_sum[1,4] >= 0.001) glm_sum[1,4] = 0.0001
	
	toselect.x <- glm_sum[,4] < 0.001

	sig.formula <- make_formula(varname,clim_test[toselect.x[-1]])  
	
	fit=glm(sig.formula,data=idata,family=quasibinomial(),...)
	plot_limitation_sneaky(fit)
	
	glm_sum=summary(fit)$coefficients
	browser()
	gdata[toselect.x,2]=glm_sum[,1]
	pdata[toselect.x,2]=glm_sum[,4]
	return(list(gdata,pdata))
}

plot_limitation_sneaky <- function(fit,idata) {
	ress=residuals(fit,type='partial')
	pred=predict(fit,type='terms')
	mods=ress+pred
	
	rastMe 	 <- function(chead) rasterFromXYZ(cbind(idata$lon,idata$lat,mods[,chead]))
	valuesMe <- function(chead)	mods[,chead]
	
	controls=sapply(c("MI","NPP","lightn"),valuesMe)
	controls=(1+exp(0-controls))
	
	controls=controls/sum(controls)
	
	plot_mans_3way(idata$lon,idata$lat,controls[,3],controls[,2],controls[,2],cols_3way,limits_3way,
 		lab=TRUE,lab_text="d) Predicted gradients")
		
}

perform_nls_comp_on_all<- function(varname,idata,gdata,pdata,...) {
	idata[idata[,"DMI"]>1,"DMI"]=1
	
	f2 <- function(x,a) 1-1/((1+x)^a)
	f3 <- function(x,a,b) 1/(1+exp(-(a*x-b)))
	f1 <- function(...) 1-f3(...)
	
	form=as.formula(paste(varname,"~ f1(DMI,a,b)*f2(Dlightn,c)*f3(NPP,d,e)"))
	
	fit=nls(form,data=idata,start=c(a=15,b=3,c=0.05,d=0.008,e=10),control = list(warnOnly=TRUE,maxiter = 500),
		trace=TRUE,lower=c(5,0,0,0.000,0),upper=c(50,30,2,1,400), algorithm="port")
		
	summ=summary(fit)$coefficients 
	gdata[,1]=summ[,1]
	pdata[,1]=summ[,4]
	gdata[,2]=summ[,1]
	pdata[,2]=summ[,4]
	return(list(gdata,pdata))
}

make_formula <- function(z,vars) {
	o.formula=paste(z,"~")
	for (i in vars) {
		o.formula=paste(o.formula,i,'+')
	}
	o.formula= as.formula(substr(o.formula,1,nchar(o.formula)-1))
	return(o.formula)
}


set_up_ouput_matrix <- function(n=2,m=0,
	colnames=c('All','significant'),rownames=c('Intercept',clim_test)) {
	
	odata=data.frame(matrix(2,max(length(clim_test)+1+m,length(rownames)),n))
	
	colnames(odata)=colnames
	rownames(odata)=rownames
	
	return(odata)
}

add_stars <- function(gdata,pdata) {
	pdata[is.na(pdata)]=2
	options("scipen"=10, "digits"=6)
	cdata= data.frame(lapply(round(gdata,6), as.character), stringsAsFactors=FALSE)
	rownames(cdata)=rownames(gdata)

	c(cdata,pdata):=add_start_for_p(cdata,pdata,0.001,'***')
	c(cdata,pdata):=add_start_for_p(cdata,pdata,0.01,'**')
	c(cdata,pdata):=add_start_for_p(cdata,pdata,0.05,'*')
	c(cdata,pdata):=add_start_for_p(cdata,pdata,0.1,'.')
	
	cdata[pdata==2]='N/A'
	return(cdata)
}

add_start_for_p <- function(cdata,odata,p,cstars) {
	cdata[odata<p]=paste(cdata[odata<p],cstars)
	odata[odata<p]=3
	
	return(list(cdata,odata))
}

write_out_pvalues <- function(odata,rname,filename_out) {
	filename_out=paste(filename_out,'-',rname,'.csv',sep="")
	write.csv(odata,filename_out)
}

