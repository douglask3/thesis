plot_changes_in_loss_cost_TS_cfg <- function(name="") {
	source('libs/return_multiple_from_functions.r')
	source('libs/make.transparent.r')
	source('libs/ma.r')
	
	cost_file 	<<- 'data/economic_loss_by_disaster.csv'
	
	cols		<<- c("Bushfire"	= "red",
					  "Cyclone" 	= "white",
					  "Earthquake"	= "green",
					  "Flood"		= "blue",
					  "Hail"		= "cyan",
					  "Storm"		= "yellow",
					  "Other"		= "grey")
	 
	yscale		<<- 1/1000000000
	png(paste("figs/disaster_cost",name,".png",sep="-"),height=7.33,width=7.33,units='in',res=150)
	layout(c(1,2,3),heights=c(1,0.3,1))
	par(mar=c(2,4.5,1,4.5),oma=rep(0.2,4))

}

plot_changes_in_loss_cost_TS <- function() {
	plot_changes_in_loss_cost_TSs("superSlim",add_smoothing=FALSE,Slim=TRUE)
	#plot_changes_in_loss_cost_TSs(add_smoothing=TRUE)
	#
	#plot_changes_in_loss_cost_TSs("10yr.mvav",RM=10)
}

plot_changes_in_loss_cost_TSs <- function(name="",add_smoothing=FALSE,Slim=FALSE,...) {
	plot_changes_in_loss_cost_TS_cfg(name)
	plot_changes_in_loss_cost_TS_plot(add_smoothing=add_smoothing,Slim=Slim,...)
	
	if (Slim) mtext("Costs (billion AU$)",side=2,xpd=TRUE,cex=0.75,line=2.5) else {
		mtext("a) Cost of Natural Disasters",adj=0)
		mtext("Costs associated with natural disaster",side=2,xpd=TRUE,cex=0.75,line=3.5)
		mtext("(billion AU$)",side=2,xpd=TRUE,cex=0.75,line=2.5)
	
	
	
		add_legend(add_smoothing)
	
		plot_changes_in_loss_cost_TS_plot(norm=TRUE,add_smoothing=FALSE,...)
		mtext("b) Normalised Cost of Natural Disaster",adj=0)
		mtext("%",side=2,xpd=TRUE,cex=0.75,line=3)
	}
	dev.off()
}
	
	
plot_changes_in_loss_cost_TS_plot <- function(add_smoothing=FALSE,Slim=FALSE,...) {

	dat=read.csv(cost_file)
	
	if (Slim) dat=dat[dat[,3]== "Bushfire",]
		
	c(x,yold,ynorm):=plot_each_catastrophe(dat,col='white',...)
	print('yay')
	y=y0=yold
	dats=split(dat[,1:2],dat[,3])
	
	for (i in rev(names(cols))) {
		yold=y
		c(x,y):=plot_each_catastrophe(dats[[i]],x,yold,ynorm,border=NA,density=70,
										col=make.transparent(cols[i],0.44),add=TRUE,...)
	}
	
	plot_each_catastrophe(dats[[i]],x,col=cols[i],ynorm=ynorm,add=TRUE,...)
	
	if (add_smoothing) {
		add_smooth_line(y0)
		add_smooth_line(yold,col=cols[i])
	}

}

plot_each_catastrophe <- function(dat,x=NULL,yold=NULL,ynorm=NULL,add=FALSE,norm=FALSE,RM=NULL,...) {
	
	xlims=range(dat[,1])
	if (is.null(x)) x=xlims[1]:xlims[2]
	
	y=sapply(x,function(yr) sum(dat[dat[,1]==yr,2]))*yscale
	x0=x
	if (!is.null(RM)) c(x,y):=find_moving_average(x+RM/2,y,RM)
	
	names(y)=x
	
	if (add) y[is.na(y)]=0
	
	if (is.null(ynorm)) ynorm=y/100
	if (!norm) ynorm=1 
	
	if (!is.null(yold)) y=(yold-y) else yold=y
	
	
	if (!add) plot(c(0,(1:length(yold))*1.2),c(0,yold/ynorm),axes=FALSE,type='n',ylab="")
	barplot(yold/ynorm,add=TRUE,xaxt='n',...)
	axis(1,at=c(0,length(yold)*1.2),tck=0,labels=c('',''))
	axis(1,at=seq(4.5,length.out=9,by=6.0),labels=seq(1970,2010,5))
	return(list(x0,y,ynorm))
}

add_smooth_line <- function(y,col="grey",RM=10) {
	
	c(x,y):=find_moving_average(((1:length(y))*1.2)-0.5,y,RM)
	lines(x,y*RM/2,col=col,cex=1.5,lty=2)
	at=axis(2)
	axis(4,at=at,labels=at*2)
	mtext("Costs associated with natural disaster",side=4,xpd=TRUE,cex=0.75,line=2.5)
	mtext("- decadeal running sum (billion AU$2014)",side=4,xpd=TRUE,cex=0.75,line=3.5)
	
}

add_legend <- function(rml=FALSE) {
	plot.new()
	mar=par("mar")
	par(mar=c(0,mar[2]/2,0,0))
	standard.legend <- function(...)
		legend(x='top',horiz=TRUE,legend=names(cols),bty='n',cex=1.15,...)
	
	standard.legend(fill='grey')
	standard.legend(fill=make.transparent(cols,0.44),density=70)
	standard.legend(fill=c(cols[1],rep("transparent",length(cols)-1)))
	
	if (rml) for (i in 1:3)
		legend(x='bottom',lty=2,col=c("grey","red"),
			  legend=c("Total cost decade running sum","Bushfire cost decade running sum"),
			  ncol=2,cex=1.15,bty='n')
	par(mar=mar)
}