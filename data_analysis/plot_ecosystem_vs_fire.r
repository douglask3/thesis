plot_ecosystem_vs_fire_cfg <- function() {
	source("libs/install_and_source_library.r")
	install_and_source_library("shapefiles")
	source("libs/make.transparent.r")

	
	filename_fire		<<- 	'data/Fire_GFEDv4_Burnt_fraction_0.5grid-nc3.nc'
	varname_fire		<<- 	'mfire_frac'
	
	filename_emissions	<<-		'data/GFED3_biomassBurning_CO2_1997-2010_20319.nc'
	varname_emissions	<<-		'GFED'
	
	eco_map				<<- 	'data/ecofloristic_zones/'
	eco_map				<<- 	'data/terr-ecoregions-TNC/'
	eco_layer 			<<- 	'tnc_terr_ecoregions'
	
	eco_key				<<-		'data/terr-ecoregions-TNC/tnc_terr_ecoregions_key.csv'
	
	rasterized_eco 		<<-		'outputs/ecoregions.grd'
	rasterized_eco 		<<-		'outputs/terr_ecoregions.grd'
	rasterized_fire		<<-		'outputs/fire.grd'
	rasterized_emissions<<-		'outputs/emissions.grd'
	
	veg_label			<<- c(	"Tropical\nWet Forest",
								"Tropical\nDry Forest",
								  "Tropical\nSavanna/Grassland",
								  "Mediterranean\nForest/\nWoodlands/Scrub",
								  "Temperate\nForest/Woodland",
								  "Boreal\nForest",
								  "Shrublands","Other")
								
	veg_groupIDs		<<-		list("Tropical Wet Forest"=c(1,12),
								  "Tropical Dry Forest"=2,
								  "Tropical Savanna/Grassland"=c(4,9),
								  "Mediterranean Forest/Woodlands/Scrub"=10,
								  "Temperate Forest/Woodland"=c(3,5,13),
								  "Boreal Forest"=16,
								  "Shrublands"=c(6,7,11))
								  
	
	hightlighted		<<-		1:14
	options("scipen" = 20)
}

open.veg.info <- function() {
	key=read.csv(eco_key,stringsAsFactors=FALSE)[,1]
	
	veg_types=as.vector(unique(key))
	
	for (i in 1:length(veg_types)) key[key==veg_types[i]]=i
	
	key=as.numeric(key)
	
	groupNames=gsub("\\d", "", names(unlist(veg_groupIDs)))
	groupID=unlist(veg_groupIDs)
	
	groupKey <- function(i) {
		index=which(groupID==i)
		if (length(index)==0) return("Other") else return(groupNames[index])
	}
	
	key=sapply(key,groupKey)
	veg_types=as.vector(unique(key))
	veg_types=veg_types[c(1,2,4,7,3,8,5)]
	return(list(key,veg_types))
}

plot_ecosystem_vs_fire <- function() {
	plot_ecosystem_vs_fire_cfg()
	
	ecoregions=open_veg_map()
	
	c(key,veg_types):=open.veg.info()
	
	fire=open_fire(filename_fire,varname_fire,rasterized_fire)
	
	emissions=open_fire(filename_emissions,varname_emissions,rasterized_emissions)#*
				#1000*60*60*24*365.24/(12*1000000000)
	
	## define the veg indexs
	key_index=1:length(veg_types)
	keynums=key
	for (i in key_index) keynums[key==veg_types[i]]=i
	
	## set ecoregion values
	ecovals=values(ecoregions)
	
	ecovals=as.numeric(keynums[ecovals-1])
	
	values(ecoregions)=ecovals
	ecoregions[is.na(ecoregions)]=0
	
	## Do th plotting
	setup_plot()
	plot_areas(ecoregions,veg_types,key_index,fire)
	legend(x=1,y=60,#horiz=TRUE,
		   legend=c("% of global land area","% of global burnt area"),
		   fill=c("#99BB99","red"),bty='n')
	mtext("  a)",adj=0,line=-3)
	plot_burnt_area(ecoregions,veg_types,key_index,fire)
	mtext("  b)",adj=0,line=-2)
	
	plot_areas(ecoregions,veg_types,key_index,emissions,plot_both=FALSE,
			   lab= "PgC/year")#,log='y')
	mtext(expression(paste("  c)")),adj=0,line=-3)
	
	
	axis(1,at=(0.5:(length(veg_label)-0.5))*1.03,labels=veg_label,srt=90,las=2,lwd=0)		   
	dev.off()
	
}



plot_areas <- function(ecoregions,veg_types,key_index,fire,plot_both=TRUE,
					   lab="%",...) {
	find_eco_area <- function(i,weight)
		sum(values(weight*area(ecoregions))[values(ecoregions)==i],na.rm=TRUE)
	
	areas =sapply(key_index,find_eco_area,!is.na(fire))
	
	fire[is.na(fire)]=0
	fareas=sapply(key_index,find_eco_area,fire)
	global_bf=sum(fareas)/sum(areas)
	
	names(areas)=veg_types
	names(fareas)=veg_types
	
	bar.stand <- function(x,col="grey",add=FALSE,...) {
		
		bs <- function(...) barplot(x,axes=FALSE,xaxt='n',width=0.85,beside=TRUE,...)
		
		bs(col="grey",add=add,...)
		col0=col
		col=make.transparent(col,0.8)
		bs(col=col,add=TRUE,...)
		col=rep("transparent",length(x))
		col[hightlighted]=col0
		bs(col=col,add=TRUE,...)
	}
	
	if(plot_both) {
		fareas=100*fareas/sum(fareas)
		fareas0=fareas
		fareas=fareas
		#fareas=10*log(5*fareas,10)
		areas=100*areas/sum(areas)
		
		bar.stand(rbind(areas,fareas),col=c('#99BB99','red'),...)
		
		print("fraction of land taken by:")
		print(areas)
		print("fraction of fire taken by:")
		print(fareas0/sum(fareas0))
		
		#axis(1,at=0.5:(length(veg_label)-0.5),labels=veg_label,srt=90,las=2,lwd=0)
	} else {
		bar.stand(fareas,col='orange',...)
		#axis(3,at=0.5:(length(veg_label)-0.5),labels=veg_label,srt=90,las=2,lwd=0,line=1)
		print("fluxes:")
		print(fareas)
		
		print("fraction offluxes:")
		print(fareas/sum(fareas))
	}
	
	axis_with_lab <- function(side,text,...) { 	
		mtext(text,side=side,line=2.5,cex=1)
		return(axis(side,...))
	}
	
	axis_with_lab(2,lab)
	
	if (plot_both) {
		#at=seq(0,26.988,length.out=7);at[1]=0.1
		#labelss=signif(10^(at/10)/5,1)
		#at=10*log(5*labelss,10)
		labelss=c(0,25,50,75,100)
		at=labelss
		
		#axis_with_lab(4,"% of global burnt area       ",at=at,labels=labelss)
	} else {
		at=c(0.1,0.3,1,3,10,30,100)
		at=seq(0,100,10)
		axis_with_lab(4,"% of global emissions",
					  at=max(fareas)*at/(100* max(fareas/sum(fareas))),labels=at)
	}
}

plot_burnt_area <- function(ecoregions,veg_types,key_index,fireR) {
	
	find_vals <- function(i,vals,logme=TRUE) {
		vals=vals[ecoregions==i]
		
		if (logme) {
			vals[!is.na(vals) & vals<0.000001]=0.000001
			vals=log(vals,10)
		}
		return(vals)
	}
	fire=lapply(key_index,find_vals,fireR,FALSE)
	
	print(paste("ecosystem burnt fraction:"))
	print(paste(veg_label,sapply(fire, mean,na.rm=TRUE)*100,"%"))
	
	fire=lapply(key_index,find_vals,fireR)
	cols=rep("white",length(fire))
	cols[hightlighted]='#FFBBBB'
	boxplot(fire,xaxt='n',yaxt='n',axes=FALSE,col=cols)
	
	axis(2,at=-6:0,labels=100*10^(-6:0))
	mtext(side=2,"Annual burnt area (%)",cex=1,line=2.5)
}

open_veg_map <- function() {
	if (!file.exists(rasterized_eco)) {
		ecoregions=readOGR(eco_map,eco_layer)
		r=raster(extent(-180,180,-90,90))
		res(r)=0.5
		ecoregions=rasterize(ecoregions,r)
		ecoregions=writeRaster(ecoregions,rasterized_eco)
	} else ecoregions=raster(rasterized_eco)
	
	return(ecoregions)
}


open_fire <- function(filename,varname,rasterized_file) {
	if (!file.exists(rasterized_file)) {
		fire=12*mean(brick(filename,varname=varname))
		fire=writeRaster(fire,rasterized_file)
	} else fire=raster(rasterized_file)
	return(fire)
}


setup_plot <- function() {
	png("figs/fire_by_biome.png",width=6,height=9,units='in',res=300)
	layout(c(1,2,3),heights=c(1,1,1))
	par(mar=c(1,4,0,4),oma=c(7.5,0,0.7,0))

}