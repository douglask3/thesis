find_max_and_min_of_highest_lowest_month <-function
    (cdata) {
    
    mdata=cdata[[c(1,1)]]
    mdata[[1]]=min(cdata)
    mdata[[2]]=max(cdata)
    
    return(mdata)
 
}