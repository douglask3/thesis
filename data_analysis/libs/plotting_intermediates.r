plot_mod_obs <- function(x,y,pdata,labs,cols,limits) {

	add_legend=FALSE
	for (i in 1:length(pdata)) {
		#if (i==length(pdata)) add_legend=TRUE
		
		plot_raster_from_vector(x,y,pdata[[i]],lon_range,lat_range,
			cols=cols,limits=limits,coast.lwd=3,
			add_legend=add_legend,ncol=2,yjust=1,bty='n',legend.pos='bottomleft',
			xjust=0.5,smooth_image=smooth_image,smooth_factor=smooth_factor)
		mtext(labs[[i]],adj=0)
	}
}

plot_mans_3way<- function(x,y,mod_lightn,mod_NPP,mod_MI,cols,limits,
						  lab=TRUE,lab_text="e) Relative Limitation Contribution",
						  add_legend=TRUE) {
						  
	out=plot_3way(x,y,mod_lightn,mod_NPP,mod_MI,lon_range,lat_range,
			  cols=cols,limits=limits,coast.lwd=3,
			  add_legend=add_legend,rvar='Ignitions',gvar='Fuel',bvar='Moisture',
			  smooth_image=smooth_image,smooth_factor=smooth_factor)
	
	if (lab) mtext(lab_text,adj=0)
	
	return(out)
}