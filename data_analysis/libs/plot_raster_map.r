plot_raster_map_cfg <- function(libs_path) {
	source(paste(libs_path,"install_and_source_library.r",sep=""))
	source(paste(libs_path,"return_multiple_from_functions.r",sep=""))
	install_and_source_library("mapproj")
	
	try(detach("package:fpc"),silent=TRUE)
	try(detach("package:mclust"),silent=TRUE)
}



plot_raster_map <- function (z,x_range=NULL,y_range=NULL,
	limits,cols,coastline=NULL,coast.lwd,
	add_legend=TRUE,legend_type='add_raster_legend2',legend.pos,
	add=FALSE,
	projection=NULL,orientation=c(0,0,0),spt.cex=1,libs_path="libs/",
	e=NULL,limits_error=c(50,100)/100,regions='.',invert_e=TRUE,e_polygon=TRUE,
	fill.ocean=TRUE,map_db="world",shimmer=FALSE,...) {
	
	plot_raster_map_cfg(libs_path)
	if (is.null(e)) limits_error=NULL
	
	if (is.null(x_range)) x_range=as.vector(extent(z))[1:2]
	if (is.null(y_range)) y_range=as.vector(extent(z))[3:4]
	
	z=crop(z,extent(x_range, y_range))
	
	if (is.null(projection)) {
		if (!is.null(limits)) z=cut_results(z,limits)
			
		image(z,col=cols[sort(unique(values(z)))],add=add)
		if (shimmer) {
			zi=z
			
			colS=make.transparent(cols,0.95)
			randy <- function() {
				shrimp=0.07
				x=runif(1,-res(z)[1]*shrimp,res(z)[1]*shrimp)
				y=sqrt(0.5*shrimp-x^2)*c(-1,1)[round(runif(1,1,2))]
				if (round(runif(1,1,2))) return(list(x,y)) else return(list(y,x))
			}
			for (i in 1:100) {
				
				c(xi,yi):=randy()
				
				perform_shimmer <- function(z,xi,yi) {
					xmin(zi)=xmin(zi)+xi
					xmax(zi)=xmax(zi)+xi
					ymin(zi)=ymin(zi)+yi
					ymax(zi)=ymax(zi)+yi
					image(zi,col=colS[sort(unique(values(z)))],add=TRUE)
				}
				
				#for (i in c(-1,1)) for  (j in c(-1,1)) {
					perform_shimmer(zi,xi,yi)
					perform_shimmer(zi,yi,xi)
					#perform_shimmer(zi,j*xi,i*yi)
					#perform_shimmer(zi,j*yi,i*yi)
				#}
				
			}
			image(is.na(disaggregate(zi,method="bilinear",fact=5)),col=c("transparent","white"),add=TRUE)
			polygon(c(-180,180,180,-180),c(rep(ymin(z)[1],2),-90,-90),col="white",border=NA)
		}
		if (!is.null(e)) add_e(e,limits_error,cols_e,invert_e,polygons=e_polygon)
	} else {
		c(x,y,z):=project_raster(z,projection,orientation)
		z=cut_results(z,limits)
		points(x,y,col=cols[z],cex=spt.cex*0.25,pch=15)
	}
	
	add_coastline(coastline,projection,orientation,lwd.coast,x_range,y_range,
				  regions=regions,fill.ocean,z,map_db=map_db)
	
	if (add_legend && !is.null(limits)) match.fun(legend_type)(cols,limits,x=legend.pos,e=limits_error,invert_e=invert_e,...)
}

add_e <- function(e,limits_error,cols_e,invert_e=TRUE,polygons=TRUE) {
	cols_e=make.transparent("grey",1-1/(length(limits_error)+1))

	if (polygons) e=disaggregate(e,5,method="bilinear")
		else e=aggregate(e,fact=4, expand=TRUE)
	e=cut_results(e,limits_error)
	
	if (invert_e) e=invert.raster(e,vals=1:(length(limits_error)+1))
	
	add_transp <- function(lim,e,pch,cex,pattern,thick,res) {	
		ee=e
		ee[e>lim]=1
		ee[e<=lim]=0
		cells=values(ee)==1
		xy=xyFromCell(ee,which(cells))
		
		cols=(e[cells]-i)/(nl-1)
		
		cols_e=make.transparent("black",0.33)
		cols=make.transparent("black",1-(0.75*cols))
		if (polygons) {
			image(pattern.raster(ee,pattern,thick=thick,res=res),col=c('transparent',cols_e),add=TRUE)
		} else points(xy,pch=pch,col=cols,cex=0.7*cex*2)
	}
	
	nl=length(limits_error)+1
	for (i in 1:(nl-1)) add_transp(i,e,
		c(4,3,1,16,5,9,11)[i],c(1,1,1.3,1.3,1.3,1,1)[i],
		c("Circle","Circle","forward-diagonal","backward-diagonal","horizontal","vertical")[i],
		c(0.1,0.67,0.5,0.5,0.5,0.5)[i],c(16,8,4,4,4,4)[i])
}

add_coastline <- function(coastline=NULL,projection=NULL,orientation=NULL,lwd.coast=1,
							x_range=c(-180,180),y_range=c(-90,90),
							fill.ocean=TRUE,rast,ocean_col="white",map_db='world',...) {
	
	if (is.null(coastline)) {
		if (fill.ocean) {
			## Just for Aus at the moment
			outline <- map(map_db, regions="Australia", exact=TRUE, plot=FALSE) # returns a list of x/y coords
			xrange <- range(outline$x, na.rm=TRUE) # get bounding box
			yrange <- range(outline$y, na.rm=TRUE)
			xbox <- xrange + c(-1, 1)
			ybox <- yrange + c(-1, 1)
			subset <- !is.na(outline$x)
			polypath(c(outline$x[subset], NA, c(xbox, rev(xbox))),
				c(outline$y[subset], NA, rep(ybox, each=2)),
				col="white",border="white", rule="evenodd")
		}
	
		if (is.null(projection)) {
			map(map_db,add=TRUE,interior=FALSE,xlim=x_range,ylim=y_range,...)
		} else {
			map(map_db,add=TRUE,interior=FALSE,projection=projection,orientation=orientation,
				xlim=x_range,ylim=y_range,...)
		}
			
	} else {
		add_coast(coastline,lwd=coast.lwd)
	}
}


project_raster <- function(z,projection='',orientation=c(0,0,0),xlength=2000,ylength=1000) {
	
	xy=xyFromCell(z,1:ncell(z))
	z=as.matrix(values(z))
	
	test=!is.na(z)
	z=z[test]
	xy=mapproject(xy[test,1],xy[test,2],projection=projection,orientation=orientation)
	x=xy[[1]]
	y=xy[[2]]
	
	xyz=cbind(x,y,z)
    
    return(list(x,y,z))
}

cut_results <- function(pdata,limits) {
	nlimits=length(limits)
	odata=pdata
	odata[]=nlimits+1
	for (i in nlimits:1) odata[pdata<=limits[i]]=i
	odata[is.na(pdata)]=NaN
	return(odata)
}	

add_raster_legend <- function(cols,limits,labelss=NULL,x='left',add=TRUE,main_title='',
						      spt.cex=3,pch=15,between=FALSE,y.intersp=1,bty='n',
						      title_line=-2,title_adj=0.06,title_cex=0.8,...) {
	
	model_legend <- function(cols,labelss,...) legend(x=x,legend=labelss,col=cols,...)
	testp=1
	bg.col="#000000"
	
	if (add==FALSE) plot.new()
	
	if (is.null(labelss)) {
		limits=round(limits,5)
		labelss=c(paste("<",limits[1]),
				  paste(limits[-length(limits)],"-",limits[-1]),
				  paste(">",limits[length(limits)]))
		if (limits[1]==0) labelss[1]='0'
	}
	
	test=(as.numeric(labelss[1])==0)
	if (length(labelss)==(length(cols)-1)) {
		labelss=c("",labelss)
	} else test=FALSE
	if (between) {
		cols=rep(cols,each=10)
		
		labelss=rep(labelss,each=10)
		labelss[(1:length(labelss))[-seq(6,length(labelss),10)]]=""
		if (test) {
			
			labelss[10]=labelss[16]
			labelss[16]=""
		}
		
		cols=cols[-(1:5)]
		labelss=labelss[-(1:5)]
		cols[(1:3)]="transparent"
		bg.col=rep(bg.col,length(cols))
		bg.col[(1:3)]="transparent"
		
		y.intersp=y.intersp/10
	}
	
	model_legend(rev(bg.col),rev(labelss),x,pt.cex=1.05*spt.cex,pch=pch,y.intersp=y.intersp,bty=bty,...)
	model_legend(rev(cols),rev(labelss),x,pt.cex=spt.cex,pch=pch,y.intersp=y.intersp,bty=bty,...)
	mtext(main_title,side=1,line=title_line+.16,cex=title_cex,adj=title_adj)
}	

add_raster_legend2 <-function(cols,limits,labelss=NULL,x='bottomleft',add=TRUE,
						      spt.cex=2,pch=15,main_title='',
						      plot_loc=c(0.22,0.78,0.07,0.10+0.05*(length(e_lims)-1)),
						      e_lims=0,labelss.cex=1,title_cex=0.8,srt=0,transpose=FALSE,
						      invert_e=TRUE,polygons=TRUE,...) {
	source("libs/transpose_cor.r")
	if (!add) {
		plot.new()
		add=TRUE
	}
	
	if (is.null(e_lims)) {
		e_lims=0
		cols_e=make.transparent("grey",1-1/(length(e_lims)+1))
	}
	
	cal_frac_of_plot_covered <- function(index=1:2) {
		xlims=par("usr")[index]
		xlims[3]=xlims[2]-xlims[1]
		return(xlims)
	}

	if (add) {
		xlims=cal_frac_of_plot_covered(1:2)
		ylims=cal_frac_of_plot_covered(3:4)
	} else {
		xlims=c(0,1,1)
		ylims=c(0,1,1)
		plot(c(0,1),c(0,1))
	}
	nlims=length(limits)+1
	
	x=xlims[1]+plot_loc[1]*xlims[3]+(plot_loc[2]-plot_loc[1])*xlims[3]*(matrix(1:nlims,nlims,1)-0.5)/nlims
	
	y=seq(ylims[1]+ylims[3]*plot_loc[3],ylims[1]+ylims[3]*plot_loc[4],length.out=length(e_lims)+1)
   	z=matrix(1:nlims,nlims,1+length(e_lims))
   	c(xx,yy,z):=transpose_cor(transpose,x,y,z)
	image(x=xx,y=yy,z=z,col=cols[1:nlims],add=TRUE,
    	  xaxt='n',yaxt='n',xlab='',ylab='',xdp=TRUE)
    	
    	
    if (length(e_lims)>1) {
    	
    	z=t(matrix(1:(1+length(e_lims)),1+length(e_lims),nlims))
    	if (invert_e) z=invert(z)
    	image_z_gt_i <- function(i,pch,cex,pattern,thick,res) {
    		zz=z
    		zz[z>i]=1
    		zz[z<=i]=0
    		
    		c(x,y,zz):=transpose_cor(transpose,x,y,zz)
    		xx=rep(x,length(y))
    		yy=rep(y,each=length(x))
    		zz=as.vector(zz)
    		cols=c('transparent',make.transparent("black",1-0.5*i/length(e_lims)))
    		cols_e=make.transparent("black",1-0.75*i/length(e_lims))
    		
    		if (polygons) {
    			zz=rasterFromXYZ(cbind(xx,yy,zz))
    			image(pattern.raster(disaggregate(zz,100),pattern,,thick=thick,res=res*25),col=c('transparent',cols_e),add=TRUE)
    		} else points(xx,yy,col=cols[zz+1],pch=pch,cex=cex*1.7)
    		#image(x=x,y=y,z=zz,col=c('transparent',cols_e),add=TRUE)
    	}
    	
    	for (i in 1:length(e_lims))
    		 image_z_gt_i(i,c(4,3,1,16,5,9,11)[i],c(1,1,1.3,1.3,1.3,1,1)[i],
							c("Circle","Circle","forward-diagonal","backward-diagonal",
							  "horizontal","vertical")[i],
							c(0.1,0.67,0.5,0.5,0.5,0.5)[i],c(16,8,4,4,4,4)[i])
    }
    dx=(x[2]-x[1])/2
    xp=c(min(x)-dx,min(x)-dx,max(x)+dx,max(x)+dx)

    yp=c(y[1]-diff(range(y))/(length(e_lims)*2),tail(y,1)+diff(range(y))/(length(e_lims)*2))
    
    c(xx,yy):=transpose_cor(transpose,xp,c(yp,rev(yp)))
    
	polygon(xx,yy,xpd=TRUE)
	
	yl=seq(yp[1],yp[2],length.out=length(e_lims)+2)
	
	if (!polygons && length(e_lims)>1) {
	if (!transpose) lapply(yl,function(y) lines(xp[2:3],rep(y,2),lwd=0.5,col=make.transparent("blacK",0.5))) 
		else lapply(yl,function(y) lines(rep(y,2),xp[2:3],lwd=0.5,col=make.transparent("blacK",0.5)))
	}
		
	#===============================
	ytop=tail(yp,1)+tail(diff(y),1)*1.4
	ybottom=yp[1]-diff(y[1:2]*1.4)
	#if (length(e_lims)>1) ytop=ybottom
	
	xt=c(x[1]-dx,x[1],x+dx)
	yt=c(ytop,rep(c(ytop,ybottom),length.out=length(xt)-1))
	
	if (is.null(labelss)) {
		labelss=c(paste("<",limits[1],sep=""),"",limits," ")#paste(limits[length(limits)],"+"))
		if (limits[1]==0) {
			labelss[1]=""
			labelss[2]='0'
			labelss[3]=""
		}
	} else {
		if(class(labelss)=='list' && length(labelss)==1) labelss=labelss[[1]]
		
		if (limits[1]==0) {
			labelss=c("",labelss[1],"",labelss[-1],"")
		} else {
			labelss=c(labelss[1],"",labelss[-1],"")
		}
		
		if (length(labelss)==(length(xt)-2)) labelss=c("",labelss,"")
		if (length(labelss)==(length(xt)-1)) labelss=c("","",labelss[-2])
	}
	if (length(labelss)==0) labelss=rep("",length(xt))
	
	
    c(xx,yy):=transpose_cor(transpose,xt,yt)
    if (transpose) srt=0
	text(x=xx,y=yy,labelss,xpd=TRUE,cex=labelss.cex,srt=srt)
	
	#===============================
	if (length(e_lims)!=1) {
		c(xx,yy):=transpose_cor(transpose,xt[1]-2*diff(xt[1:2]),c(y[1]-diff(y[1:2]),y)-0.33*c(diff(y[1:3]),diff(y))/2)
		srt=0
		if (transpose) srt=-90
		
		text(x=xx,y=yy-0.1,
			 c(0,e_lims,paste(tail(e_lims,1),"+")),pos=3,cex=labelss.cex,xpd=TRUE,srt=srt)
		
		mtext("sd/mean (%)",side=c(2,1)[transpose*1+1],cex=title_cex,line=-1+transpose*1.0)
	}
	
	for (i in list(c(1,3),c(2))[[transpose*1+1]]) 
		mtext(main_title,side=i,cex=title_cex,line=-1-transpose*0.3)
		
	
}
add_coast <- function(shape_file="libs/AusCoast.csv",...) {

	
	hcoast=read.csv(shape_file)
	#hcoast[,4]=hcoast[,4]+.25
	
	for (isle in 1:max(hcoast[,2])) {
		test=hcoast[,2]==isle
		lines(hcoast[test,3],hcoast[test,4],...)
	}
}

