plot_raster_from_raster <-function(z,x_range=NULL,y_range=NULL,
							   	   limits=seq(min(z),max(z),10),cols=rainbow(length(limits)),
								   coastline=NULL,coast.lwd=par("lwd"),
								   add_legend=TRUE,legend_type='add_raster_legend2',legend.pos='bottomleft',
								   smooth_image=TRUE,smooth_factor=5,
								   projection=NULL,orientation=NULL,libs_path="libs/",
								   e=NULL,e_polygon=TRUE,regions = "Aus",fill.ocean=TRUE,
								   quick=FALSE,add=TRUE,...) {
	
	source(paste(libs_path,"install_and_source_library.r",sep=""))
	source(paste(libs_path,"plot_raster_map.r",sep=""))
	install_and_source_library("raster")
	install_and_source_library("maps")
	install_and_source_library("mapproj")
	install_and_source_library("mapdata")
	
	try(detach("package:fpc"),silent=TRUE)
	try(detach("package:mclust"),silent=TRUE)
	
	if (is.null(x_range)) x_range=c(xmin(z),xmax(z))
	if (is.null(y_range)) y_range=c(ymin(z),ymax(z))
	
	if (length(cols)!=(length(limits)+1)) cols=make_col_vector(cols,ncols=length(limits)+1)

	if (quick) {
		smooth_image=FALSE
		fill.ocean=FALSE
		e_polygon=FALSE
		map_db="worldHires"
	} else map_db='world'
		
	mar=par("mar")
	
	#plot(x=range(x_range), y=range(y_range), type='n', axes=FALSE, ann=FALSE)
	if (is.null(projection)) {
		
		map(map_db,interior=FALSE, xlim=range(x_range),ylim=range(y_range),regions=regions,mar=par("mar"))
	} else {
		map(map_db,projection=projection ,interior=FALSE, orientation=orientation,
			xlim=range(x_range),ylim=range(y_range),regions=regions)
	}
		
	if (smooth_image) z=disaggregate(z,smooth_factor,method="bilinear")

	plot_raster_map(z,x_range,y_range,limits,cols,coastline,coast.lwd,
		add_legend,legend_type,legend.pos,add=add,projection=projection,orientation=orientation,
		libs_path=libs_path,e=e,regions=regions,fill.ocean=fill.ocean,e_polygon=e_polygon,
		map_db=map_db,...)
	par(mar=mar)
}


