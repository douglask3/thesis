open_mod_data <- function(filename,varname) brick.gunzip(filename,varname=varname)

glob_mean <- function(i,hdata,harea=NULL,tarea=NULL) {
	print(i)
	if (is.null(harea)) harea=area(hdata[[i]],na.rm=TRUE)
	if (is.null(tarea)) tarea=sum(values(harea),na.rm=TRUE)
		
	tot=sum(values(hdata[[i]]*harea),na.rm=TRUE)
	return(tot/tarea)
}

glob_mean_double_list <- function(ldata,months,...) {
	gm=list()
	
	for (i in 1:length(ldata)) {
		gmi=list()
		for (j in 1:length(ldata[[i]])) {
			gmi[[j]]=sapply(months[1]:months[2],glob_mean,ldata[[i]][[j]],...)
		}
		
		gm[[i]]=gmi
	}
	return(gm)
}


stack_sums <- function(rdata,startp=1,endp=nlayers(rdata),average=TRUE) {

    if (length(startp)==1) startp=startp:endp
    print(startp[1])
	odata=rdata[[startp[1]]]
	
	for (i in startp[-1]) {
		print(i)
		odata=odata+rdata[[i]]
	}
	
	if (average) odata=odata/length(startp)
	
	return(odata)
}

clip_data <- function(hdata,fdata,e) {
	hdata=mem.safe.crop(hdata,e)
	fdata=clip_double_list_raster(fdata,e)
	return(list(hdata,fdata))
}

clip_double_list_raster <-function(ldata,e) {
	for (i in 1:length(ldata)) {
		for (j in 1:length(ldata[[i]])) {
			ldata[[i]][[j]]=mem.safe.crop(ldata[[i]][[j]],e)
		}
	}
	return(ldata)
}

mask_double_list_raster <-function(ldata,mask) {
	for (i in 1:length(ldata)) {
		for (j in 1:length(ldata[[i]])) {
			ldata[[i]][[j]]=ldata*mask
		}
	}
	return(ldata)
}	
	


mem.safe.crop <<- function(rdata,e,...) {
	progress_tempN()
	return(crop(rdata,e,filename=temp.filename(),overwrite=TRUE))
}

mask_data <- function(hdata,fdata,mask) {
	hdata=hdata*mask
	fdata=mask_double_list_raster(fdata,mask)
	return(list(hdata,fdata))
}

progress_tempN <- function() {
 	if (!exists("tempN")) tempN=0
	tempN	<<- tempN+1
}

temp.filename <- function() {
	if (!file.exists("temp_rasters/")) dir.create("temp_rasters")
	paste("temp_rasters/temp_rasters",tempN,sep="")
}

writeRaster.mem.safe <- function(pdata)
	writeRaster(pdata,filename=temp.filename(),overwrite=TRUE)
	
TandHarea <- function(x) {
	a=area(x[[1]],na.rm=TRUE)
	a[x[[1]]==-999]=0
	return(list(a,sum(values(a),na.rm=TRUE)))
}

swap_raster<- function(pdata,i=1,in.memory=TRUE) {
    pdata=pdata[[i]]
	idata=as.matrix(pdata)
	
	idata1=idata
	idata1[,1:360]=idata[,361:720]
	idata1[,361:720]=idata[,1:360]
	values(pdata)=idata1
	if(extent(pdata)==extent(-180,180,-90,90)) {
		extent(pdata)=extent(0,360,-90,90)
	} else if(extent(pdata)==extent(0,360,-90,90)) {
		extent(pdata)=extent(-180,180,-90,90)
	}
	
	if (!in.memory) {
		progress_tempN()
		pdata=writeRaster.mem.safe(pdata)
	}
	
	return(pdata)
}

is.raster.class <- function(r) {
	classi=class(r)
	if (classi=="Raster" || classi=="RasterStack" || classi=="RasterBrick" ||
	    classi=="RasterLayer") return(TRUE)
	return(FALSE)
}

layer.apply <- function(indexs,FUN,...) {
 	if (class(FUN)!="function") FUN <- match.fun(FUN) 
 	
 	if (is.raster.class(indexs)) {
		x=FUN(indexs[[1]],...)
		if (!is.raster.class(x))  x=list(x)
		if (nlayers(indexs)>1) {
			if (is.raster.class(x)) {
				for (i in 2:nlayers(indexs)) x=addLayer.ext(x,FUN(indexs[[i]],...))
			} else {
				for (i in 2:nlayers(indexs)) x[[i]]=FUN(indexs[[i]],...)
			}
		}
	} else {
		
		x=FUN(indexs[1],...)
		for (i in indexs[-1]) {
			y=FUN(i,...)
			if (!is.null(y)) {
				if (is.null(x)) x=y else x=addLayer.ext(x,y)
			}
		}
 	}
 	return(x)
}

addLayer.ext <- function(a,b) {
	
	if(!extent(a)==extent(b)) {
		a=crop(a,b)
		b=crop(b,a)
	}
	return(addLayer(a,b))
}

layer.sum <-function(...) sum(...)
layer.min <-function(...) min(...)
layer.max <-function(...) max(...)

layer.min.climateology <- function(r,n=12,na2zero=TRUE,...) {
	if(na2zero) r[is.na(r)]=0
	return(min(layer.climateology(r,n,...),...))
}

layer.climateology <- function(r,n=12,...) {
	l=nlayers(r)
	month.mean <- function (i) mean(r[[seq(i,l,12)]],...)
	return(layer.apply(1:n,month.mean))
}



area.sum 		<- function(rast) sum(values(area.rast.by.rast(rast)),na.rm=TRUE)

area.monthly.sum<- function(rast) 
	unlist(layer.apply(rast,function(i) sum(values(area.rast.by.rast(i)),na.rm=TRUE)))

area.max 		<- function(rast) area.sum.FUN (rast,layer.max)

area.min 		<- function(rast) area.sum.FUN (rast,layer.min)

area.sum.FUN 	<- function(rast,FUN) sum(values(FUN(area.rast.by.rast(rast))),na.rm=TRUE)

area.land 		<- function(rast) {
	if (nlayers(rast)==1) rast=rast[[1]]
	sum(values(area.rast1.by.rast2(rast,!is.na(rast))),na.rm=TRUE)
}

area.sum.monthly.mean <- function(rast) area.monthly.sum(rast)/area.land(rast)

area.sum.mean	<- function(rast) area.sum(rast)/area.land(rast)

area.sum.mean.monthly.spread <- function(...) rep(area.sum.mean(...),12)


area.min.mean	<- function(rast) area.min(rast)/area.land(rast[[1]])


area.mean.sum 	<- function(rast) mean(values(area.rast.by.rast(rast)),na.rm=TRUE)

area.mean.mean 	<- function(rast) area.mean.sum(rast)/area.land(rast)


area.rast.by.rast <- function(rast) area.rast1.by.rast2(rast,rast)

area.rast1.by.rast2 <- function(rast1,rast2) {
	if (nlayers(rast2)==1) rast2=rast2[[1]]
	return(area(rast1)*rast2)
}

layer.sd <- function(ldata,pmean=FALSE) {

    llayers=nlayers(ldata)
    ones=rep(1,llayers)
    lmean=stackApply(ldata,ones,'mean',na.rm=TRUE)
    ldelt=ldata-lmean
    ldelt=ldelt*ldelt
    
    lvarn=stackApply(ldelt,ones,'sum',na.rm=TRUE)/llayers
    
    if (pmean) lvarn=sqrt(lvarn)/lmean
    return(lvarn)
    
}

FUN.gunzip <- function(FUN,filename,lon_range=NULL,lat_range=NULL,...) {  
	print(filename)
	install_and_source_library("R.utils")
	ext=substr(filename,nchar(filename)-2,nchar(filename))
	if (ext=='.gz' ||  ext=='zip') {
	
		if(ext=='.gz') filename_unzip=substr(filename,1,nchar(filename)-3)  else 
			filename_unzip=substr(filename,1,nchar(filename)-4) 
		
		
		try(gunzip(filename,destname=filename_unzip))
		
		out=FUN(filename_unzip,...)
		
		if (nlayers(out)==1) out=out[[1]]
		out=out*1
		try(gzip(filename_unzip))
	} else {
		out=FUN(filename,...)
		if (nlayers(out)==1) out=out[[1]]
		out=out*1
	}
	
	if (!is.null(lon_range) && !is.null(lat_range))
		out=crop(out,extent(lon_range[1],lon_range[2],lat_range[1],lat_range[2]))
	return(out)
}

raster.gunzip <- function(...) FUN.gunzip (raster,...)
stack.gunzip  <- function(...) FUN.gunzip (stack,...)
brick.gunzip  <- function(...) FUN.gunzip (brick,...)

raster.out.add <- function(raster.out,add,...) filename.add(raster.out,add,ext=".grd",...)


max.raster <- function(a,...) max(values(a),...)
min.raster <- function(a,...) min(values(a),...)
range.raster <- function(a,...) range(values(a),...)

unique.raster <- function(a,na.rm=TRUE,...) {
	a=unique(values(a),...)
	if (na.rm) a=a[!is.na(a)]
	return(a)
}