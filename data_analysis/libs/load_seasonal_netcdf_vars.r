load_seasonal_netcdf_vars <-function(filename,varname,average=FALSE,
  	annual=TRUE,seasonal=TRUE,max_min=TRUE,negative=FALSE,
  	climateologies=FALSE,startp=1,endp=NULL,return_monthlies=TRUE) {
    
	source("libs/find_annual_average.r")
    source("libs/find_seasonal_concentration_and_phase.r")
    source("libs/find_max_and_min_of_highest_lowest_month.r")
	source("libs/install_and_source_library.r")
	install_and_source_library("raster")
    
    print("opening netcdf")
    cdata=brick(filename,varname=varname)
    
    if (climateologies) {
    	if (is.null(endp)) endp=nlayers(cdata)
    	cdata=cdata[[startp:endp]]
    	mdata=cdata[[1:12]]
    	
    	for (i in 1:12) {
    		print(paste("making climatology. Month:",i))
    		mindex=seq(i,nlayers(cdata),12)
    		mdata[[i]]=0
    		for (j in mindex) {
    			mdata[[i]]=mdata[[i]]+cdata[[j]]/length(mindex)
    		}
    	}
    	cdata=mdata
    }
    
	if (negative) {
		cdata=max(cdata)-cdata
	}
    
    annual_average=NaN
    phase=NaN
    conc=NaN 
    minv=NaN
    maxv=NaN 
    monthlies=NaN  
    
    if (annual) {
      print("calcuating annual average")
      annual_average=find_annual_average(cdata,average)
	  
    }
    
    if (seasonal) {
      print("calculating seasonal values")
      sdata=find_seasonal_concentration_and_phase(cdata)
	  phase=sdata[[1]]
	  conc=sdata[[2]]
    }
    
    if(max_min)   {
      print("calculating mins and maxs")
      mdata=find_max_and_min_of_highest_lowest_month(cdata)
	  minv=mdata[[1]]
	  maxv=mdata[[2]]
    }
    
    if (return_monthlies) {
    	monthlies=cdata
    }
    
    
    return(list(annual_average=annual_average,
		phase=phase,conc=conc,
		minv=minv,maxv=maxv,
		monthlies=monthlies))
		
    
}
