load_glm_results <- function(mod_colname,mod_filename) {
	mod_results=read.csv(mod_filename,row.names=1)[mod_colname]

	rnames=rownames(mod_results)
	
	indexs=which(mod_results[,1]!="N/A")
	mod_results=mod_results[indexs,1]
	rnames=rnames[indexs]
	
	mod_results=as.numeric(unlist(strsplit(as.character(mod_results)," "))
		[seq(1,by=2,length.out=length(mod_results))])
	mod_results=matrix(as.numeric(mod_results),length(mod_results),1)

	rownames(mod_results)=rnames
	return(mod_results)
	
}


load_clim_data <- function(filename,cols,monthly=FALSE) {
	if (monthly) cols=c('Jan','Feb','Mar','Apr','May','Jun',
					'Jul','Aug','Sep','Oct','Nov','Dec')
					
	xy=read.csv(filename,row.names=1)[,2:1]

	clim_data=read.csv(filename,row.names=1)[,cols]
	return(list(xy,clim_data))
}

load_nclim_data	<- function(filename,varname,startp=1,endp=NULL) {
	cdata=brick(filename,varname=varname)
	if (is.null(endp)) endp=nlayers(cdata)
	
	cdata=cdata[[startp:endp]]
	
	e=extent(lon_range[1], lon_range[2], lat_range[1], lat_range[2])
	
	return(crop(cdata,e))
}