quantile.raster90 <- function(Cdat) {
	slength=Cdat[[1]]
	values(slength)=unlist(apply(values(Cdat),1,quantile.range))
	return(slength)
}

quantile.raster90.cell <- function(i) {
	i=sort(i,TRUE)
	if (sum(!is.na(i))==0 || sum(i)==0) return(NaN)
	
	season=sum(i)*0.9
	a=0
	for (m in 1:12) {
		a=a+i[m]
		if (a> season) return(m)
	}
	return(12)
}