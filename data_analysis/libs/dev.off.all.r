dev.off.all <- function() {
    test=0
    while (class(test)!="try-error") {
      test=try(dev.off(),silent=TRUE)
    }
}
