find_seasonal_concentration_and_phase <- function(cdata,phase_units="degrees")
{
    library("raster")
    a=dim(cdata)
    
    xdata=cdata[[1]]
    xdata[,]=0
    ydata=xdata
    adata=xdata
    
    for (k in 1:a[3]) {
        angle=2*pi*(12-k+1)/12
        xdata=xdata+cdata[[k]]*cos(angle)
        ydata=ydata+cdata[[k]]*sin(angle)
        adata=adata+cdata[[k]]
    }
    
    pdata=atans(0-ydata,0-xdata,phase_units)
    cdata=sqrt(xdata^2+ydata^2)/adata
    list(pdata,cdata)
}

atans <- function(x,y,units)
{
    phase_out=x[[1]]
    
    x=as.matrix(x)
    y=as.matrix(y)
    
    phase=atan(x/y)
    
    test=(x>0)+(y<0);
    phase[which(test==2)]=pi+phase[which(test==2)];

    test=(x==0)+(y<0);
    phase[which(test==2)]=pi;

    test=(x<0)+(y<0);
    phase[which(test==2)]=-pi+phase[which(test==2)];

    test=(x<0)+(y==0);
    phase[which(test==2)]=-pi/2;

    if (units=='months') {
      phase=6*(phase/pi)+6;
    } else if (units=='degrees') { 
      phase=phase+pi
      phase=phase*360/(2*pi)
    }
            
    values(phase_out)=phase
    return(phase_out)
}
