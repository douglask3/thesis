make_col_vector <- function(r,g,b,ncols,libs_path="libs/") {
	source(paste(libs_path,"install_and_source_library.r",sep=""))
	install_and_source_library("colorspace")
	
	if (class(r)=="character") {
		col=col2rgb(r)
		r=col[1,]/255
		g=col[2,]/255
		b=col[3,]/255
	}

	col_vec <- function(a) approx(seq(1,ncols,length.out=length(a)),a,1:ncols)$y

	cols=RGB(col_vec(r),col_vec(g),col_vec(b))

	return(hex(cols))

}

