load_lpx_results <- function(filename,obs,obs_xy,blur_mod=TRUE) {
	mod=raster(filename,varname='afire_frac')

	if (blur_mod) mod=blur_raster(mod)

	obs=rasterFromXYZ(cbind(obs_xy[,1],obs_xy[,2],obs))
	
	mod=crop(mod,extent(obs))
	
	mod=values(mod)
	obs=values(obs)
	mod=mod[is.na(obs)==FALSE]
	
	
	return(list(obs_xy,mod))
}

blur_raster <- function(x,sigma=1) {

	y=values(x,format='matrix')
	
	z=blur(im(y),sigma=sigma)
	z[is.na(y)]=NaN

	values(x)=as.vector(t(as.matrix(z)))
	
	return(x)
	
}