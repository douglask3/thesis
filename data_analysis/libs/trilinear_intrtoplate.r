trilinear_intrtoplate <- function(means,x) {
	a=dim(means)
	
	c(p,d):=find_min_value(means[x[1]:1,x[2]:1,x[3]:1])
	
	c(p[2],d[2]):=find_min_value(means[x[1]:a[1],x[2]:1,x[3]:1])
	c(p[3],d[3]):=find_min_value(means[x[1]:1,x[2]:a[2],x[3]:1])
	c(p[4],d[4]):=find_min_value(means[x[1]:1,x[2]:1,x[3]:a[3]])
	
	c(p[5],d[5]):=find_min_value(means[x[1]:a[1],x[2]:a[2],x[3]:1])
	c(p[6],d[6]):=find_min_value(means[x[1]:a[1],x[2]:1,x[3]:a[3]])
	c(p[7],d[7]):=find_min_value(means[x[1]:1,x[2]:a[2],x[3]:a[3]])
	c(p[8],d[8]):=find_min_value(means[x[1]:a[1],x[2]:a[2],x[3]:a[3]])

	if (sum(is.na(p))==8) {
		p=NaN
	} else {
		p=sum(p*d,na.rm=TRUE)/sum(d,na.rm=TRUE)
	}
	
	return(p)
	
}

find_min_value <- function(m) {

	b=dim(m)
	b[b>4]=4
	m=m[1:b[1],1:b[2],1:b[3]]
	d=m
	
	x=y=z=d
	
	x[,,]=1:b[1]
	y[,,]=rep(1:b[2],each=b[1])
	z[,,]=rep(1:b[3],each=b[1]*b[2])
	
	d=sqrt(x^2+y^2+z^2)
	
	d[is.na(m)]=10^5
	min.d=min(d)
	
	m=m[d==min.d]
	p=sum(m)/length(m)
	
	return(list(p,min.d))
}