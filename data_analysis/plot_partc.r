##################################################################
## Configuration                                                ##
##################################################################
configure_plot_abundances <-function() {
    
    source("libs/make_transparent.r")
    source("libs/install_and_source_library.r")
    source("libs/dev.off.all.r")
    source("libs/return_multiple_from_functions.r")
    install_and_source_library("ncdf")
    install_and_source_library("ncdf4")
    install_and_source_library("stats")
    
    dev.off.all()
    ##Observations
    obs_remote_filename <<- "data/veg_cont_fields_CRU.nc"
    obs_fire_filename  	<<- '../../barkthickness_database/data/Fire_GFEDv4_Burnt_fraction_0.5grid9.nc'
    
    obs_tree_varname    <<- "Tree_cover"
    obs_grass_varname   <<- "Herb"
    obs_fire_varname   	<<- "mfire_frac"
    
    ##  Modelled data:
    LPX_fpc_varname     <<- 'fpc_grid'
    LPX_fpc_filename    <<- '../../Outputs_eg/r184_mod_bench_cont-5143.nc'
	

    LPX_fire_varname  	<<- 'mfire_frac'
    LPX_fire_filename  	<<- LPX_fpc_filename

    ## Plot info
    startyr             <<- 5143
    nyrs                <<- 1
    
    xlabel_name         <<- "Burnt fraction (%)"           
    ylabel_name         <<- 'Cover (%)' 
    alpha               <<- 0.995
    figure_name         <<- 'figs/fpc_vs_fire.png'
    figure_height       <<- 3.25
    figure_width        <<- 6*3.25/4
    
    x_range             <<- c(-5.3,0)
    x_labs				<<- -5:0
    y_range				<<- c(0,95)
        
    nbins               <<- 100
    npolygons           <<- 334
    bin_width           <<- 0.1
    
    tree_colour         <<- "darkgoldenrod4"
    tree_LPX_colour    	<<- "burlywood3"
    grass_colour        <<- "darkgreen"
    grass_LPX_colour   	<<- "green"
    
    just_trees			<<- TRUE
    just_tree_col		<<- c("#008FAA","#AA8F00")
    
    options("scipen"=100, "digits"=4)
}

##################################################################
## Main function                                                ##
##################################################################

plot_abundances <- function() {
    configure_plot_abundances()
    
    if (just_trees) {
    	tree_colour <<- just_tree_col[1]
    	tree_LPX_colour <<- just_tree_col[2]
    }
    ## set up window
    set_up_window()
    
    set_and_plot_remote_observations()
    plot_LPX_runs() 
    
    add_x_label()
    add_y_label(adj=3.5-3*just_trees)
    dev.off()
}
##################################################################
## Gubbins                                                      ##
##################################################################
set_and_plot_remote_observations <- function () {

	if (just_trees) {
    	txt=c("Observed","Simulated")
  		cols=c(tree_colour,tree_LPX_colour)
    } else {
    	txt=c("tree - obs","grass - obs")
    	cols=c(tree_colour,grass_colour)
    }
    
    set_up_plot(cols,txt,xaxt="n",leg.x=-2,leg.y=80)
  
    plot_observations()
    if (!just_trees) {
    	mtext('Observed',side = 3,line=-2,adj=0.01,cex=1.33)
   		mtext('Fractional cover vs. fire',side = 3,line=0.5,cex=1.67)
	}
}


##################################################################
## Set up overall window                                        ##
##################################################################   
set_up_window <- function() {
	if (!just_trees) figure_height=figure_height*2
	
    png(figure_name, height=figure_height, width=figure_width,
        units = "in",res=300)
    
    if (!just_trees) layout(1:2,heights=c(1,1))
    
    par(oma=c(3,0,2,0),mar=c(0,4,1,1))

}   

##################################################################
## Scripts for setting plots                                    ##
##################################################################
set_up_plot <- function(plot_colours,label,plot_clark=FALSE,leg.x=-2,leg.y=90,...) {
        
    plot(x_range,y_range,xaxs='i',yaxs='i',
    xlim=x_range,ylim=y_range,
        xlab="",ylab="",
        type = "n", cex=.5,...) 
    
    
    axis(1,labels=FALSE)
    add_legend(plot_colours,label,x=leg.x,y=leg.y)
    
}

add_y_label <- function(adj=0.5)  mtext(ylabel_name,side = 2,line=2,adj=adj,cex=1.2)


add_x_label <- function() {
	
    mtext(xlabel_name,side = 1,line=2,adj=0.5,cex=1.2)
	axis(1,at=x_labs,labels=100*10^(x_labs))
}

add_legend <- function(plot_colours=c('red','blue','black'),
    label=c('    RS', '    OS','    NR'),x=-2,y=90) {
    
    quant_colours <- make.transparent(plot_colours,alpha)
    
    standard_legend(quant_colours,lwd=0,label=label,x=x,y=y)
    
    label=rep(' ',length(label))
    for (lwd in seq(1,25,25/100)) {
        standard_legend(quant_colours,lwd=lwd,label=label,x=x,y=y)
    }
    standard_legend(plot_colours,lwd=2,label=label,x=x,y=y)
}

standard_legend <- function(coloursz,lwd=1,lty=1,
    label=c(' ',' ',' '),x=-2,y=90) {

  legend(x=x,y=y,legend=label,
         border=FALSE,col=coloursz,bty="n",xpd=TRUE,
         lwd=lwd,lty=lty,ncol=1)
}

##################################################################
## Sets up for plotting the lines and shades areas for          ##
##      each plot                                               ##
##################################################################
plot_observations <- function(cal_yr,use_index=FALSE) {
    
    tree=load_LPX_data(obs_remote_filename,obs_tree_varname)
 	grass=load_LPX_data(obs_remote_filename,obs_grass_varname)    
    fire=load_LPX_data(obs_fire_filename,obs_fire_varname,sump=TRUE)/17.75
	
	
    test=is.na(tree)+is.na(grass)+is.na(fire)+(tree<0)+(grass<0)+((tree+grass)<75)
    test=test==0
        
    tree=as.vector(tree[test])
    grass=as.vector(grass[test])
    fire=as.vector(fire[test])
    
    fire=log(fire,10)
    fire[fire<x_range[1]]=NaN
    tot=(tree+grass)
    tree=100*tree/tot
    grass=100*grass/tot
    
    line_shade_plot(tree,fire,tree_colour,use_index=use_index) 
    if (!just_trees) line_shade_plot(grass,fire,grass_colour,use_index=use_index)
}

plot_LPX_runs <-function(use_index=FALSE) {
	if (!just_trees)
		set_up_plot(c(tree_LPX_colour,grass_LPX_colour),
            		c("tree - LPX","grass - LPX"),xaxt="n",leg.x=-2,leg.y=80)
    
    years=startyr+(1:nyrs)-1
    
    TR=matrix(0,720,360)
    GS=TR
    fire=TR
    
    for (i in years) {
        c(TR0,GS0,nn):=load_LPX_cover_data(i,
            LPX_fpc_filename,LPX_fpc_varname,c(1:7),8:9,1:9)
        TR=TR+TR0/nyrs
        GS=GS+GS0/nyrs
        fire=fire+load_LPX_fire_data(i,LPX_fire_filename)/nyrs
    }    

    test=is.na(TR)+is.na(GS)+is.na(fire)
    test=test==0
    TR=as.vector(TR[test])
    GS=as.vector(GS[test])
    fire=as.vector(fire[test])
    
    TR=TR#/(TR+GS)
    
	fire=log(fire,10)
	fire[fire<x_range[1]]=x_range[1]

	if (just_trees) {
		line_shade_plot(TR,fire,tree_LPX_colour,use_index=use_index)
	} else {
		line_shade_plot(TR,fire,tree_LPX_colour,use_index=use_index)
		line_shade_plot(GS,fire,grass_LPX_colour,use_index=use_index)
	}
    
}




##################################################################
## Plot the line and shaded area                                ##
##################################################################

line_shade_plot <- function(Y,X,colour,ycutoff=NULL,use_index=FALSE,
    lwd=2,...) {
    c(x,y,e):=binning(Y,X) 

    if (use_index) c(y,nn):=abundance_index(y,e)
	
    e[is.na(e)]=0
    test=which(is.na(y)==FALSE)
    x=x[test]
    y=y[test]
    
    e=e[test,]
    
    lines(x,y,col=make.transparent(colour,0.3),lwd=2,...)
    
    x=c(x,rev(x))
    
    ii=round(npolygons/2)+1
    
    for (i in (round(npolygons/2)+1):npolygons) {
        ii=ii-1

        yp=c(e[,i],rev(e[,ii]))
        polygon(x,yp,col=make.transparent(colour,alpha),border=NA)
    }
    polygon(x,yp,col=make.transparent(colour,alpha),border=NA)
    if (is.null(ycutoff)==FALSE) {
        mask_upper_or_lower_plot(x,y,ycutoff[1],FALSE)
        mask_upper_or_lower_plot(x,y,ycutoff[2],TRUE)
    }
}

binning <- function(Y,X) { 

    dbin=(x_range[2]-x_range[1])/nbins
    bins=seq(x_range[1],x_range[2],dbin)
    ymean=rep(NaN,nbins)
    yerr=matrix(NaN,nbins,npolygons)
    
    for (i in 1:nbins) {
        r1=bins[i]-bin_width/2
        r2=bins[i+1]+bin_width/2
        test=(X<r2)+(X>r1)
        
        if (sum(test==2,na.rm=TRUE)>3) {
            ymean[i]=median(Y[test==2],na.rm=TRUE)
            
            ps=(seq(0.25,1,length.out=(npolygons/2+1))^.4/2)[-(npolygons/2+1)]
            ps=c(ps,0.5+(0.5-rev(ps)))
            
            yerr[i,]=quantile(Y[test==2],probs=ps,na.rm=TRUE)
                
        }
    }
    x=bins[1:nbins]+dbin/2
    return(list(x,ymean,yerr))
}

##################################################################
## Loading netcdf data                                          ##
##################################################################

load_LPX_cover_data <-function(year,filename,varname,
    G1_pfts,G2_pfts,G3_pfts) {

    print(paste("   opening year:",year))
    n=nchar(filename)
    substr(filename,n-nchar(as.character(year))-2,n-3)= as.character(year)
    print(filename)
    fpc=load_LPX_data(filename,varname)
    
    G1=rowSums(fpc[,,G1_pfts],dim=2)*100
    G2=rowSums(fpc[,,G2_pfts],dim=2)*100
    G3=rowSums(fpc[,,G3_pfts],dim=2)*100

    return(list(G1,G2,G3))

}

load_LPX_fire_data <- function(year,filename) {
    fire=load_LPX_data(filename,'mfire_frac',yr=year)
    return(rowSums(fire,dim=2))
}
    

load_LPX_data <-function(filename,varname,swap=FALSE,flip=FALSE,
    yr=NULL,sump=FALSE) {
    
    if (is.null(yr)==FALSE) {
        filename=construct_filename(filename,yr)
    }

    nc=open.ncdf(filename)
    
    vdata=get.var.ncdf(nc, nc$var[[varname]])
    
    
    if (sump) if (length(dim(vdata))>2) vdata=rowSums(vdata,dims=2)
    
    if (swap) {
        vdata=swap_halves(vdata,yr)
    }
    if (flip) {
        vdata=flip_data(vdata,yr)
    }
    
    return(vdata)

}

construct_filename <- function(filename,year) {
    n=nchar(filename)
    
    substr(filename,n-nchar(as.character(year))-2,n-3)= as.character(year)
    print(filename)
    return(filename)
}

swap_halves <- function(vdata,yr=1) { 
        
        vdata=extract_dim(vdata,yr)

        a=dim(vdata)
        vdatai=vdata
        vdata[1:floor(a[1]/2),]=vdatai[(ceiling(a[1]/2)+1):a[1],]
        vdata[(floor(a[1]/2)+1):a[1],]=vdatai[1:ceiling(a[1]/2),]
        return(vdata)
}

flip_data <- function(vdata,yr=1) {
    extract_dim(vdata,yr)

    a=dim(vdata)
    vdata[,a[2]:1]=vdata[,1:a[2]]
    return(vdata)
}

extract_dim <- function(vdata,yr) {

    if (length(dim(vdata))==3) {
        vdata=vdata[,,yr]
    }
    
    return(vdata)
}

##################################################################
## Converting cover to index                                    ##
##################################################################

abundance_index <- function(cover,error) {
    min_cover=min(cover,na.rm=TRUE)    
    cover=cover-min_cover
    error=error-min_cover
    
    max_cover=max(cover,na.rm=TRUE)
    cover=100*cover/max_cover
    error=100*error/max_cover
    
    return(list(cover,error))
}