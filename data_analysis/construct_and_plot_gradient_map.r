##########################################################################################
## Configure																			##
##########################################################################################
construct_and_plot_gradient_map_setup <- function() {
	source("libs/install_and_source_library.r")
	source("plot_glm_results.r")
	source("libs/return_multiple_from_functions.r")
	source("libs/loading_glm_and_clin_outputs.r")
	source("libs/plotting_intermediates.r")
	source("libs/trilinear_intrtoplate.r")
	
	clim_file		<<-	"outputs/AusClim.csv"
	
	grad_dims		<<- c("NPP","MI","lightn")
	
	glm_filename	<<- 'outputs/fire_glm-All_products.csv'
	
	obs_name		<<- "AVHRR.All"
	obs_colname		<<- "AVHRR"
	
	mod_results		<<- list(c('data/lpxr184/lit_review_control_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_litter_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_MI_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_lightn_and_lightn_67b62dd-10156.nc'),
						   	 c('data/lpxmv1rs/lit_review_control_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_litter_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_lightn_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_MI_and_lightn_9f2a880-10156.nc'))
	unit_bf			<<- 0.33
	splits			<<- 10
	window_size		<<-	3
	table_no_thresh <<- 1
	cols_3way		<<- rev(c("FF","DF","BF","9F","6F","3F"))	
	limits_3way		<<- c(0.33,0.5,0.67)
	dev.off.all()
	
}
##########################################################################################
## Gubbins																				##
##########################################################################################
construct_and_plot_gradient_map <- function() {
	construct_and_plot_gradient_map_setup()
	plot_glm_results_stepup()
	construct_and_plot_gradient_map_setup()
	
	png("figs/gradient.png",height=12,width=4,units = "in",res=600)
	par(mfcol=c(4,1),mar=c(0,0,2,0),oma=c(1,1,1,1))
	
	c(obs_xy,obs):=load_clim_data(clim_file,obs_colname)
	c(xy,clim):=load_clim_data(clim_file,grad_dims)
	
	cunits=calculate_units(grad_dims)/splits
	
	print(cunits)
	
	c(x,means):=construct_array(clim,cunits)
	xp=find_x_locs(cunits,clim,x)
	c(means0,nums):=populate_table(means,xp,obs,0,1)
	c(means,nums):=populate_table(means,xp,obs,window_size,table_no_thresh)
	
	mod=back_calculate_obs_from_means(means0,xp)
	
	plot_mod_obs(xy[,1],xy[,2],list(obs,mod),
		list("a) Observations","b) Mean modelled"),
			cols_bf,limits_bf)
			
	plot_mod_obs(xy[,1],xy[,2],list(mod-obs),
		list("c) Modelled - Observations"),
			cols_dbf1,limits_dbf1)

	grad=sapply(1:3,find_derivative,means,nums,xp)
	grad=abs(grad)
	
	
	plot_mans_3way(xy[,1],xy[,2],.5*grad[,3],grad[,2],2.0*grad[,1],cols_3way,limits_3way,
 		lab=TRUE,lab_text="d) Predicted gradients")
	
	dev.off()
	
	png("figs/gradient_limitation.png",height=6*1.33,width=7.5*1.33,units = "in",res=600)
		par(mfcol=c(1,1),mar=c(0,0,2,0),oma=c(1,1,1,1))
		plot_mans_3way(xy[,1],xy[,2],.5*grad[,3],grad[,2],2.0*grad[,1],cols_3way,limits_3way,
 			lab=FALSE)
 	dev.off()
}

##########################################################################################
## Calculate unit of each climate variable												##
##########################################################################################

calculate_units <- function(glm_results) {
	glm_results=load_glm_results(obs_name,glm_filename)[grad_dims,]
	
	calculate_unit <- function(grad) {

		if(grad<0) {
			cunit=(1/(grad))*log(unit_bf/(1-unit_bf))
		} else {
			unit_bf=1-unit_bf
			cunit=(1/(grad))*log(unit_bf/(1-unit_bf))
		}

		return(cunit)
	}
	
	cunits=sapply(glm_results,calculate_unit)
	return(cunits)
}

##########################################################################################
## Construct 3way climate space array													##
##########################################################################################
construct_array <- function(clim,cunits) {

	set_axis <- function(i,clim,cunits)
			seq(0-3*cunits[i],3*cunits[i]+max(clim[,i]),cunits[i])
			
	x=lapply(1:3,set_axis,clim,cunits)

	means=array(0,dim=c(length(x[[1]]),length(x[[2]]),length(x[[3]])))
	
	return(list(x,means))
}


find_x_locs <- function (cunits,clim,x) {

	find_x_loc <- function (y,x,cunit) {
		test=((x<y)+(x>y-cunit))==2
		p=(1:length(x))[test]
		return(p)
	}
	
	find_x_locs <- function(i,clim,x,cunits) {
		p=sapply(clim[,i],find_x_loc,x[[i]],cunits[i])
	}
	
	xp=sapply(1:3,find_x_locs,clim,x,cunits)
	
	return(xp)
}

##########################################################################################
## Fill climate space array with observations											##
##########################################################################################
populate_table <- function(means,xp,obs,window_size=1,table_no_thresh) {
	nums=means
	a=dim(means)
	index <- function(i,a,xp) max(c(xp[i]-window_size,1)):min(c(xp[i]+window_size,a[i]))
	
	for (i in 1:length(obs)) {
		indexs=lapply(1:3,index,a,xp[i,])

		means[indexs[[1]],indexs[[2]],indexs[[3]]]=
			means[indexs[[1]],indexs[[2]],indexs[[3]]]+obs[i]
		nums[indexs[[1]],indexs[[2]],indexs[[3]]]=nums[indexs[[1]],indexs[[2]],indexs[[3]]]+1
	}
	
	means=means/nums
	means[nums<table_no_thresh]=NaN

	return(list(means,nums))
}

##########################################################################################
## Reconstruct fire from climate space array observations								##
##########################################################################################
back_calculate_obs_from_means <- function(means,xp) {
	mod=means[xp]
	
	test=is.na(mod)

	for (i in (1:length(xp[,1]))[test]) {
		mod[i]=trilinear_intrtoplate(means,xp[i,])
	}
	
	return(mod)
	
}
##########################################################################################
## Find partial derivatives																##
##########################################################################################
find_derivative <- function(cdim,means,nums,x) {
	print(cdim)
	xp=x[,cdim]
	xU=xD=x
	ldim=dim(means)[cdim]
	
	grad=rep(NaN,length(xp))
	
	xpU=xp+1
	xpU[xpU>ldim]=NaN
	xpD=xp-1
	xpD[xpD<1]=NaN
	
	xU[,cdim]=xpU
	xD[,cdim]=xpD
	
	return_means <- function(i,means,x) means[x[i,1],x[i,2],x[i,3]]
	
	d0	=sapply(1:length(xp),return_means,means,x)
	dmax=sapply(1:length(xp),return_means,means,xU)
	dmin=sapply(1:length(xp),return_means,means,xD)
	
	test=((!is.na(dmax))+(!is.na(dmin)))==2
	grad[test]=(dmax[test]-dmin[test])/2
	
	test=((!is.na(dmax))+(is.na(dmin)))==2
	grad[test]=dmax[test]-d0[test]
	
	test=((is.na(dmax))+(!is.na(dmin)))==2
	grad[test]=d0[test]-dmin[test]
	
	
	test=is.na(grad)

	for (i in (1:length(xp))[test]) {
		U=trilinear_intrtoplate(means,xU[i,])
		D=trilinear_intrtoplate(means,xD[i,])
		grad[i]=(U-D)/2
	}
		
	return(grad)
}

