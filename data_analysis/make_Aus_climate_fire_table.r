## make_Aus_climate_fire_table															##
##########################################################################################
## define inputs files 																	##
##########################################################################################
## make Aus 0.5 grid outputs for:
## Moisture: MAP;PC;IAVpr;MI;
## Production: fapar;faparC;IAVfapar;
## Ignitions: lightn;lightnC;IAVlightn


## lightning to be added later

##########################################################################################
## define i/o files 																	##
##########################################################################################
## variables with annual average only													##
filename_MI		='data/moisture_index.nc'
filename_IAVpr	='data/precip_IAV_ANU_aus.nc'
filename_NPP 	='data/NPP.nc'
# filename_IAVfapar

varname_MI		='MI'
varname_IAVpr	='IAV'
varname_NPP		='NPP'
# varname_IAVfapar

## variables with annual average and seasonal info										##
filename_precip	='data/ANUCLIM_precip_climatology.nc'
filename_fapar	='data/EVI_fapar_climatology.nc'
filename_lightn ='data/lit_review_litter_and_lightn_9f2a880-10156.nc'

varname_precip	='pr'
varname_fapar	='fapar'
varname_lightn	='mlightn_eff'

## annual average burnt area															##
filename_GFED3	= 'data/fire_GFED.nc'
filename_GFED4	= 'data/Fire_GFEDv4_Burnt_fraction_0.5grid-nc3.nc'
filename_AVHRR	= 'data/ff_AVHRR_global.nc'

varname_GFED3	= 'mfire_frac'
varname_GFED4 	= 'mfire_frac'
varname_AVHRR	= 'layer'

## output file 																			##
output_file		= 'outputs/AusClim.csv'
output_mprecip_file = 'outputs/Monthly_precip.csv'


##########################################################################################
## Source functions and libs 															##
##########################################################################################
source("libs/load_seasonal_netcdf_vars.r")
source("libs/return_multiple_from_functions.r")
source("libs/install_and_source_library.r")
install_and_source_library("raster")
   
   
##########################################################################################
## Read in data																			##
##########################################################################################
## read in annual avergage variables													##
MI=raster(filename_MI,varname_MI)
IAVpr=raster(filename_IAVpr,varname_IAVpr)
AVHRR=raster(filename_AVHRR,varname_AVHRR)
NPP=raster(filename_NPP,varname_NPP)

# read in monthly varaibles
GFED3=load_seasonal_netcdf_vars(filename_GFED3,varname_GFED3,FALSE,
                                TRUE,FALSE,FALSE,
                                climateologies=TRUE,startp=7,endp=114)
                                
GFED4=load_seasonal_netcdf_vars(filename_GFED4,varname_GFED4,FALSE,
                                TRUE,TRUE,FALSE,
                                climateologies=TRUE,startp=7,endp=114)
    
                            
## read in monthly variables															##
prec=load_seasonal_netcdf_vars(filename_precip,varname_precip,FALSE,
                               TRUE,TRUE,TRUE,return_monthlies=TRUE)
							   
fapar=load_seasonal_netcdf_vars(filename_fapar,varname_fapar,TRUE,
                                TRUE,TRUE,TRUE)
                                
lightn=load_seasonal_netcdf_vars(filename_lightn,varname_lightn,TRUE,
                                TRUE,TRUE,TRUE)

##########################################################################################
## project all into common raster														##
##########################################################################################
mproject_and_find_values <- function(i,var,raster_grid) {
	project_and_find_values(var[[i]],raster_grid)
}

project_and_find_values <- function(var,raster_grid,return_xy=FALSE) {
	vres=res(var)[1]
	rres=res(raster_grid)[1]
	
	if (rres>vres*2) {
		var=aggregate(var,fact=rres/(vres*2))
	}
	
	var=resample(var,raster_grid)
	xy=xyFromCell(var,1:length(values(var)))
  	var=values(var)
  	
  	tvar=values(raster_grid)
  	var=var[which(tvar==1)]
  	
  	if (return_xy) {
  		
  		return(list(xy[which(tvar==1),1],xy[which(tvar==1),2],var))
  	} else {
  		return(var)
  	}
}

mask=(is.na(GFED3$annual_average))==0

MI=project_and_find_values(MI,mask)
IAVpr=project_and_find_values(IAVpr,mask)

MAP=project_and_find_values(prec$annual_average,mask)
PC=project_and_find_values(prec$conc,mask)

mprecp=sapply(1:12,mproject_and_find_values,prec$monthlies,mask)


faparC=project_and_find_values(fapar$conc,mask)
fapar=project_and_find_values(fapar$annual_average,mask)

lightnC=project_and_find_values(lightn$conc,mask)
lightn=project_and_find_values(lightn$annual_average,mask)

NPP=project_and_find_values(NPP,mask)

GFED4A=project_and_find_values(GFED4$annual_average,mask)
GFED4P=project_and_find_values(GFED4$phase,mask)
GFED4=GFED4A

AVHRR=project_and_find_values(AVHRR$annual_average,mask)
c(lon,lat,GFED3):=project_and_find_values(GFED3$annual_average,mask,return_xy=TRUE)


DMI=MI*(1-PC)
Dlightn=lightn*(1-PC)

##########################################################################################
## Outputs																				##
##########################################################################################
AusClim <- data.frame(lat,lon,MAP,PC,IAVpr,MI,DMI,
					  lightn,lightnC,Dlightn,
                      fapar,faparC,NPP,
                      GFED3,GFED4,GFED4P,AVHRR)
                      
mprecp=cbind(lat,lon,mprecp)
colnames(mprecp)=c('lat','lon','Jan','Feb','Mar','Apr','May','Jun',
					'Jul','Aug','Sep','Oct','Nov','Dec')
               
test=rep(TRUE, dim(AusClim)[1])                   
for (i in 1:dim(AusClim)[1]) {
	if ( (sum(is.na(AusClim[i,]))+sum(AusClim[i,c(-1,-2)]<0,na.rm=TRUE))>0) {
		test[i]=FALSE
	}
}

AusClim =AusClim[test,] 
mprecp=mprecp[test,]              
write.csv(AusClim,output_file)
write.csv(mprecp,output_mprecip_file)



     
