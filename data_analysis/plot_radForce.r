min.base=4

convert2Log <- function(x) {
	#x=(log(abs(x)/10,10)+min.base)*x/abs(x)
	#x[is.na(x)]=0
	return(x)
}

convertFromLog <- function(x) {
	#x=10^(x*x/abs(x)-min.base)*10*x/abs(x)
	#x[is.na(x)]=0
	return(x)
}

text2line <- function(string,...) {
	if (class(string)!="expression") {
		string=strsplit(as.character(string)," ")[[1]]
		if (!is.even(length(string))) string=c(string,"")
		string=paste(apply(matrix(string,2),2,function(i) paste(i,collapse=" ")),
				 collapse="\n")
	}
	text(labels=string,...)
}

png('figs/radForce.png',height=10*3/4,width=7.5,units='in',res=200)
dat=read.csv('data/radiative_forcing.csv')
plot(c(0.0000,nps+2),c(-1,1.8),axes=FALSE,type='n',xlab="",ylab="")
nps=length(unique(dat[,1]))
#text(5.7,0.05,"indirect",srt=0,cex=0.67,col='red')
lines(c(0,8),c(0,0),lty=2,lwd=2)
at=seq(-1,1.75,by=0.5)

labelss=signif(convertFromLog(at),2)
at=convert2Log(labelss)
axis(2,at=at,labels=labelss)
mtext(expression(paste("Radiative forcing (W m"^-2,")"),sep=""),side=2,line=2.67)

labelss[is.na(labelss)]=0
axis(2,at=at,labels=labelss)

plot_it <- function(dat,fill,i,ymin=0,tit=FALSE) {
	
	#dat=dat[dat[,5]!=0,]
	if (nrow(dat)==0) return()
	if (dat[1,5]>=0) col=as.character(dat[,3]) else  col=as.character(dat[,4] )
	cols=rbind(make.transparent(col,0.77),col)
	if (!fill) cols=apply(cols,2,rev)
	
	polyy <- function(y,ymin,cborder,cfill,titles,srt,dx,dy) {
		if (y==0) return()
		y0=y
		x=c(i-0.3+fill*0.05,i+0.3-fill*0.05)
		if (y<0) y=c(-ymin,y) else y=c(ymin,y)
		if (abs(y[2])<abs(y[1])) y[2]=y[2]+y[1]
		if (y[2]<0 && y[1]>0) y[1]=0
		if (y[2]>0 && y[1]<0) y[1]=0
		y=convert2Log(y)
		
		if (sum(diff(y))==0) return()
		polygon(c(x,rev(x),x[1]),c(rep(y,each=2),y[1]),border=cborder,col=cfill)
		
		if (cborder=="transparent") col=cfill else col=cborder
		if (tit) text2line(x=i+dx,y=mean(y)+dy,srt=srt,string=titles,cex=0.67,col=col)
	}
	tot=sum(dat[,5])
	if (dim(dat)[1]>1) for (p in (2:dim(dat)[1])) dat[p,5]=dat[p,5]+dat[p-1,5]
	
	if (length(dat[,5])>1) ymin=ymin+c(0,dat[-dim(dat)[1],5])
	
	mapply(polyy,(dat[,5]),(ymin),(cols[1,]),(cols[2,]),dat[,2],dat[,6],dat[,7],dat[,8])
	
	if (fill) y=0.17*tot/abs(tot) else y=convert2Log(tot)-0.17*tot/abs(tot)
	#text(i,y,signif(tot,2),cex=0.8,font=2)
	return(ymin)
}

plot_source <- function(dat,...) {
	sapply(split(dat,dat[,5]<0),plot_it,...)
	
} 

plot_type <- function(dat,i,...) {
	ys=plot_source(dat[,c(1:5,7:9)],fill=FALSE,i,tit=TRUE,...)
	plot_source(dat[,c(1:4,6:9)],fill=TRUE,ymin=ys,i,...)
	string=as.character(dat[1,1])
	if (string=="Stratospheric water-vapour from CH4") string=expression("Stratospheric\nwater vapour from CH"[4]) else
		if (string=="CO2") string=expression("CO"[2]) else
			if (string=="CH4") string=expression("CH"[4]) else
				if (string=="N2O") string=expression(paste("N"[2],"O"))
	
	text2line(x=i,y=-1.1,string=string,srt=90,adj=0)
	
}

ord=dat[,1]
dat=split(dat,dat[,1])

mapply(plot_type,dat[c(4,3,5,6,7,8,9,1,2)],(1:nps)-0.8)

dev.off()