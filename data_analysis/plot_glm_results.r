##########################################################################################
## Configure																			##
##########################################################################################

plot_glm_results_stepup <- function() {
	source("libs/return_multiple_from_functions.r")
	source("libs/dev.off.all.r")
	source("libs/plot_raster_from_vector.r")
	source("libs/plot_3way.r")
	source("libs/loading_glm_and_clin_outputs.r")
	source("libs/plotting_intermediates.r")
	source("libs/load_lpx_results.r")
	
	source("libs/install_and_source_library.r")
	install_and_source_library("spatstat")
	
	dev.off.all()
	
	
	clim_data_file	<<- 'outputs/AusClim.csv'
	
	#mod_colname		<<- c("AVHRR.All","GFED4.All","GFED3.All")
	#data_colname	<<- c("AVHRR",	  "GFED4",    "GFED3")
	
	mod_colname		<<- c("AVHRR.All",	"AVHRR.All",	"lpxr184",		"lpxMv1rs"  )
	data_colname	<<- c("AVHRR",	  	"AVHRR",		"LPX",    		"LPXM-v1-rs")
	type			<<- c("glm",		"nls",			"lpx",			"lpx"		)
	
	mod_results		<<- list('outputs/fire_glm-All_products.csv',
							 'outputs/fire_nls-All_products.csv',
						   c('data/lpxr184/lit_review_control_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_litter_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_MI_and_lightn_67b62dd-10156.nc',
						   		'data/lpxr184/lit_review_lightn_and_lightn_67b62dd-10156.nc'),
						   	c('data/lpxmv1rs/lit_review_control_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_litter_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_lightn_and_lightn_9f2a880-10156.nc',
						   		'data/lpxmv1rs/lit_review_MI_and_lightn_9f2a880-10156.nc'))
	
	lon_range 		<<- c(110,155)
	lat_range		<<- c(-45,-10)
	
	cols_bf			<<- c('#FFFFFF',"#FFD0BF","#FF9A86",
							"#E04422","#C02200","#AA1100","#550000","#110000")
	
	limits_bf 		<<- c(0.001,0.01,0.02,0.05,0.1,0.2,0.5)
	
	cols_dbf1		<<- c("#000033","#0040CD","#364AFF","#90AAFF",'#FFFFFF',"#FFAA90","#FF4A36",
							"#CD4000","#330000")
	
	limits_dbf1		<<- c(-0.1,-0.05,-0.02,-0.01,0.01,0.02,0.05,0.1)
	
	cols_dbf2		<<- cols_bf #c('#FFFFFF',"#FFEAD0","#FFBA96",
							#"#FF7020","#ED3D09","#DA3300","#990000","#330000")
	
	limits_dbf2		<<- c(0.001,0.01,0.02,0.05,0.1,0.2,0.5)
		
	cols_3way		<<- rev(c("FF","DF","BF","9F","6F","3F"))	
	limits_3way		<<- c(0.33,0.5,0.67)
	
	smooth_image	<<- TRUE
	smooth_factor	<<- 5
	
	
	manipulation_exp_file	<<- "figs/glm_manipulations"
	limitation_file			<<- "figs/limitation"
	threeWay_nc_file		<<- "outputs/limitation"
}

##########################################################################################
## Gubbins																				##
##########################################################################################

plot_manipulation_results <- function() {
	
	plot_glm_results_stepup()

	nmods=length(mod_colname)
	
	png(paste(limitation_file,".png",sep=""),
		height=5*nmods,width=7.5,units = "in",res=600)
	par(mfcol=c(nmods,1),mar=c(0,0,0,0),oma=c(1,1,2,1),cex=1)
	sapply(4,plot_individual_result)
	
	#dev.off()
}

plot_individual_result <- function(i) {
	png(paste(manipulation_exp_file,"-",mod_colname[i],"-",type[i],".png",sep=""),
		height=15,width=7.5,units='in',res=600)
	par(mfcol=c(5,2),mar=c(0,0,2,0),oma=c(1,1,1,1))
	
	c(obs_xy,obs):=load_clim_data(clim_data_file,data_colname[1])
	
	c(xy,mod,mod_NPP,mod_MI,mod_lightn):=open_or_make_model_results(i,obs,obs_xy)

	plot_mod_obs(xy[,1],xy[,2],list(obs,mod,mod_NPP,mod_MI,mod_lightn),
		list("a) Observations","b) GLM result","c) Maximised NPP",
			"d) Minimised moisture","e) Maximised ignitions"),
			cols_bf,limits_bf)
		
	mod_NPP=mod_NPP-mod
	mod_MI=mod_MI-mod
	mod_lightn=mod_lightn-mod
	mod=mod-obs
	
	mod_NPP
	mod_lightn
	mod_MI=mod_MI
	
	plot_mans_3way(xy[,1],xy[,2],mod_lightn,mod_NPP,mod_MI,cols_3way,limits_3way)
	
	plot_mod_obs(xy[,1],xy[,2],list(mod),
		list("f) GLM - Observations"),
			cols_dbf1,limits_dbf1)
	
	plot_mod_obs(xy[,1],xy[,2],list(mod_NPP,mod_MI,mod_lightn),
		list("g) Change with maximised NPP",
			"h) Change with minimised moisture","i) Change with maximised ignitions"),
			cols_dbf2,limits_dbf2)
	
	dev.off()

	png(paste(limitation_file,"-",mod_colname[i],"-",type[i],".png",sep=""),
			height=6*1.33,width=7.5*1.33,units = "in",res=600)	
		par(mfcol=c(1,1),mar=c(0,0,2,0),oma=c(1,1,1,1))	
		plot_mans_3way(xy[,1],xy[,2],mod_lightn,mod_NPP,mod_MI,cols_3way,limits_3way,
			lab=FALSE)
	dev.off()
	
	add.legend=FALSE
	if (i==3) add.legend=TRUE
	
	out=plot_mans_3way(xy[,1],xy[,2],mod_lightn,mod_NPP,mod_MI,cols_3way,limits_3way,
		lab=FALSE,add_legend=add.legend)
	mtext(data_colname[i],adj=0,cex=2)
	
	writeRaster(out,filename=paste(threeWay_nc_file,"-",mod_colname[i],"-",type[i],".nc",sep=""),
				varname='limitation',longname='lightning; NPP; MI limitations',
				format='CDF',overwrite=TRUE)
	
}

##########################################################################################
## Open and make glm, or call open LPX results function									##
##########################################################################################

open_or_make_model_results <- function(i,obs,obs_xy) {
	
	if (type[i]=="glm" || type[i]=="nls") {
		glm_results=load_glm_results(mod_colname[i],mod_results[[i]])
	
		if (type[i]=='glm') {
			c(xy,clim_data):=load_clim_data(clim_data_file,rownames(glm_results)[-1])
			mod				=make_stats_result(glm_results,clim_data,type[i])
		
			mod_NPP			=make_glm_maniulation(glm_results,clim_data,'NPP',
								max(clim_data$NPP),mod)
			mod_MI 			=make_glm_maniulation(glm_results,clim_data,'MI' ,0,mod)
			mod_lightn		=make_glm_maniulation(glm_results,clim_data,'lightn', 
								3*max(clim_data$lightn),mod)
		} else {
			c(xy,clim_data):=load_clim_data(clim_data_file,c("DMI","Dlightn","NPP"))
			mod				=make_stats_result(glm_results,clim_data,type[i])
			
			#mod_NPP			=make_nls_maniulation_NPP   (glm_results,clim_data)
			#mod_MI			=make_nls_maniulation_MI    (glm_results,clim_data)
			#mod_lightn		=make_nls_maniulation_lightn(glm_results,clim_data)
			mod_NPP			=make_stats_result(glm_results,clim_data,type[i],dN=100)
			mod_MI			=make_stats_result(glm_results,clim_data,type[i],dM=-0.01)
			mod_lightn		=make_stats_result(glm_results,clim_data,type[i],dL=100)
		}
		
	} else if(type[i]=="lpx") {
		c(nn,mod)			:=load_lpx_results(mod_results[[i]][1],obs,obs_xy)
		
		c(nn,mod_NPP)		:=load_lpx_results(mod_results[[i]][2],obs,obs_xy)
		c(nn,mod_MI)		:=load_lpx_results(mod_results[[i]][3],obs,obs_xy)
		c(xy,mod_lightn)	:=load_lpx_results(mod_results[[i]][4],obs,obs_xy)
		
		mod_lightn=mod+(mod_lightn-mod)/2
		mod_NPP=mod+(mod_NPP-mod)*3
	}
	
	return(list(xy,mod,mod_NPP,mod_MI,mod_lightn))
}
##########################################################################################
## Make and manipulate GLM results														##
##########################################################################################

stats_mod_setup <- function(clim_data) {
	source("fire_construction_from_stats_model.r")
	fire_construction_from_stats_model_setup()

	clim_data=do_transformation(clim_data)
	return(clim_data)
}

make_stats_result <- function(mod_results,clim_data,type,...) {
	clim_data=stats_mod_setup(clim_data)
	
	if (type=='glm') mod=glm_model(mod_results,clim_data,...)
	if (type=='nls') mod=nls_model(mod_results,clim_data,...)
	return(mod)
}

glm_model <- function(mod_results,clim_data) {
	mod=rep(mod_results[1],dim(clim_data)[1])
	for (i in 1:dim(clim_data)[2]) mod=mod+clim_data[,i]*mod_results[i+1]
	return(1/(1+exp(-mod)))
}


#f1 <- function(x,a) 1-x^a
#f2 <- function(x,a) 1-1/((1+x)^a)
#f3 <- function(x,a,b) 1/(1+exp(-(a*x-b)))

f2 <- function(x,a) 1-1/((1+x)^a)
f3 <- function(x,a) 1/(1+exp(-(a[1]*x-a[2])))
f1 <- function(x,a) 1-f3(x,a)

sep_for_nls <- function(clim_data) {
	DMI=clim_data[,"DMI"]
	NPP=clim_data[,"NPP"]
	Dlightn=clim_data[,"Dlightn"]
	DMI[DMI>1]=1
	return(list(DMI,NPP,Dlightn))
}

nls_model <- function(mod_results,clim_data,dM=0,dL=0,dN=0) {
	c(DMI,NPP,Dlightn):=sep_for_nls(clim_data)
	return(f1(DMI+dM,mod_results[1:2])*f2(Dlightn+dL,mod_results[3])*f3(NPP+dN,mod_results[4:5]))
}

make_glm_maniulation <- function(mod_results,clim_data,clim,value,mod0) {
	clim_data[[clim]]=value
	mod=make_stats_result(mod_results,clim_data,"glm")
	#mod[mod<mod0]=mod0[mod<mod0]
	return(mod)
}

make_nls_maniulation_MI <- function(mod_results,clim_data) {
	c(DMI,NPP,Dlightn):=sep_for_nls(clim_data)
	return(f2(Dlightn,mod_results[3])*f3(NPP,mod_results[4:5]))
}

make_nls_maniulation_lightn <- function(mod_results,clim_data) {
	c(DMI,NPP,Dlightn):=sep_for_nls(clim_data)
	return(f1(DMI,mod_results[1:2])*f3(NPP,mod_results[4:5]))
}

make_nls_maniulation_NPP <- function(mod_results,clim_data) {
	c(DMI,NPP,Dlightn):=sep_for_nls(clim_data)
	return(f1(DMI,mod_results[1:2])*f2(Dlightn,mod_results[2]))
}


