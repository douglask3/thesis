##########################################################################################
## Configure																			##
##########################################################################################

plot_nls_graph_stepup <- function() {
	source("libs/return_multiple_from_functions.r")
	source("libs/dev.off.all.r")
	source("libs/loading_glm_and_clin_outputs.r")
	source("libs/load_lpx_results.r")
	source("plot_glm_results.r")
	
	source("libs/install_and_source_library.r")
	install_and_source_library("spatstat")
	
	dev.off.all()
	
	clim_data_file	<<- 'outputs/AusClim.csv'
	mod_results		<<- 'outputs/fire_nls-All_products.csv'
	figname			<<- 'figs/limiation_lines.png'
	
	mod_colname		<<- "AVHRR.All"
	data_colname	<<- "AVHRR"
	
	NPP				<<- c(0,1200)
	DMI				<<- c(0,0.4)
	Dlightn			<<- c(0,200)
	
	NPP.at			<<- seq(0,2000,300)
	DMI.at			<<- seq(0,0.6,0.05)
	Dlightn.at		<<- seq(0,400,40)
	
	NPP.sc			<<- 1
	DMI.sc			<<- 0.5
	Dlightn.sc		<<- 1
	
	NPP.title		<<- expression(paste("Net Primary Production (gC/m"^"2",")",sep=""))
	DMI.title		<<- "Dry season alpha"
	Dlightn.title	<<- expression(paste("Dry season Lightning Strikes (Strikes/km"^"2",")",sep=""))
	
	
	Nx				<<- 1000
	wbin			<<-	0.005
	
	moist_ex		<<- c(0.15,0.3)
	fuel_frag		<<- 200
	
	moist_ex.col	<<- 'blue'
	fuel_frag.col	<<- '#00AA00'
	
	standard.lwd	<<- 2
	vertical.lwd	<<- 3
	dotted.lwd		<<- 4
	points.cex		<<- 0.1
}
##########################################################################################
## Gubbins																				##
##########################################################################################

plot_nls_graph <- function() {
	
	plot_nls_graph_stepup()
	setup_figure()

	glm_results=load_glm_results(mod_colname,mod_results)
	c(nn,obs):=load_clim_data(clim_data_file,data_colname[1])
	c(nn,clim_data):=load_clim_data(clim_data_file,c("DMI","Dlightn","NPP"))
	
	clim_data[,1]=clim_data[,1]/DMI[2]
	clim_data[,2]=clim_data[,2]/Dlightn[2]
	clim_data[,3]=clim_data[,3]/(0.45*NPP[2])
	
	do.plot<- function(FUN,txt,...) {
		setup_plot()
		FUN(...)
		mtext(txt,side=2,line=2)
		add_shaded(moist_ex/max(DMI),moist_ex.col,0.75)
		add_dotted(fuel_frag/max(NPP),fuel_frag.col,dotted.lwd)
	}
	
	do.plot(plot_lines,"Burnt fraction",glm_results,obs,clim_data)
	do.plot(plot_gradients,"Normalised Gradient",glm_results)
	
	cols=c("blue","red","green")
	legend(0,-0.1,c("Dry season moisture","Dry season lightning","Net Primary Production"),
		   col=cols,xpd=NA,lty=rep(1,3),lwd=standard.lwd,bty='n')
		   
	legend(0.33,-0.1,c("","    Moving Max.",""),
		   col=cols,xpd=NA,pch=19,pt.cex=points.cex*6,bty='n')
	
	legend(0.7,-0.1,c("Moisture of extinction","    Albini (1976)",
					  "Fuel fragmentation","    point (Schultz, 1988)"),
		   lty=c(0,0,3,0),lwd=c(0,0,dotted.lwd,0),pch=c(15,0,0,0),
		   pt.cex=c(points.cex*50,0,0,0),
		   col=c(make.transparent(moist_ex.col,0.75),
		   	     "transparent",fuel_frag.col,"transparent"),xpd=NA,bty='n')
		   
	dev.off()
	
}

##########################################################################################
## Plotting straight lines																##
##########################################################################################


plot_lines <- function(glm_results,obs,clim_data) {
	
	plot_result(DMI,f1,glm_results[1:2],'blue',obs,clim_data,list(f2,f3),
			    list(glm_results[3],glm_results[4:5]),c("DMI","Dlightn","NPP"),0.2,
			    DMI.at,DMI.title,sc=DMI.sc)
	plot_result(Dlightn,f2,glm_results[3],'red',obs,clim_data,list(f1,f3),
			    list(glm_results[1:2],glm_results[4:5]),c("Dlightn","DMI","NPP"),2.85,
			    Dlightn.at,Dlightn.title,sc=Dlightn.sc)
	plot_result(NPP,f3,glm_results[4:5],'green',obs,clim_data,list(f1,f2),
			    list(glm_results[1:2],glm_results[3]),c("NPP","DMI","Dlightn"),5.5,
			    NPP.at,NPP.title,sc=NPP.sc)
}


plot_result <- function(x,f,a,col,obs,clim_data,fs,As,varnames,line,at,txt,...) {
	plot_line(x,f,a,col,...)
	#plot_partials(obs,clim_data,fs,As,varnames,col)
	plot_maximums(obs,clim_data,varnames[[1]],col,...)
	
	labels=at
	if (labels[1]==0) labels[1]=""
	axis(1,line=line+1.25,at=at/x[2],labels=labels,col=col,mgp=c(2,0.5,0),lwd.ticks=0.5)
	mtext(txt,side=1,line=line+0.3)
}

plot_line <- function(x,f,a,col,plotT=TRUE,sc=1) {
	
	x=sequance.me(x)
	y=f(x,a)
	xlab=x
	x=x/(max(x))
	if (plotT) lines(x*sc,y,col=col,lwd=standard.lwd)
	return(list(x,y))
}

plot_partials <- function(obs,clim_data,fs,As,varnames,col) {
	x=clim_data[[varnames[1]]]
	
	varnames=varnames[-1]
	scales=1
	
	for (i in length(fs)) scales=scales*fs[[i]](clim_data[[varnames[i]]],As[[i]])	
	obs=obs/scales
	points(x,obs,cex=points.cex,pch=19,col=col)
}

plot_maximums <- function(obs,clim_data,varnames,col,sc=1) {
	
	x=clim_data[[varnames]]
	
	xbins=seq(0+wbin/2,10,wbin)
	y=rep(NaN,length(xbins))
	names(y)=xbins
	for (i in xbins) {
		test=x>(i-wbin/2) & x<(i+wbin/2)
		if (sum(test)>0) y[as.character(i)]=quantile(obs[test],0.9)
	}
		
	points(xbins*sc,y,cex=points.cex*6,col=col,pch=19)
}
	

sequance.me <- function(a) seq(a[1],a[2],length.out=Nx)

add_shaded<- function(x,col,alpha)
	polygon(c(x,rev(x)),c(-1,-1,2,2),col=make.transparent(col,alpha),border=NA)


add_dotted <- function(x,col,lwd) lines(rep(x,2),c(-1,2),col=col,lwd=lwd,lty=3)



##########################################################################################
## Plotting gradients lines																##
##########################################################################################

plot_gradients <- function(glm_results) {

	plot_gradient(DMI,f1,glm_results[1:2],'blue',"",sc=DMI.sc)
	plot_gradient(Dlightn,f2,glm_results[3],'red',expression(paste("       Strikes/km"^"2",sep="")),sc=Dlightn.sc)
	plot_gradient(NPP,f3,glm_results[4:5],'green',expression(paste("       g/m"^"2",sep="")),sc=NPP.sc)
}


plot_gradient <- function(x,f,a,col,units,sc=1) {
	scaled=max(x)
	c(x,y):=plot_line(x,f,a,plotT=FALSE)
	x=x*sc
	x=x[-1]+diff(x)
	dy=abs(diff(y))
	
	lines(x,dy/max(dy),col=col,lwd=standard.lwd)
	
	maX=x[which.max(dy)]
	lines(rep(maX,2),c(-1,2),col=col,lwd=vertical.lwd)
	text(x=maX+0.02,y=0.75,c(paste(signif(maX*scaled,3),sep=""),units),srt=-90,adj=0)
}

##########################################################################################
## Plotting setup															##
##########################################################################################
setup_figure <- function() {
	png(figname,height=8,width=8,units='in',res=300)
	par(mfrow=c(2,1),mar=c(8,4,0.5,0.5))
	
}

setup_plot <- function()
	plot(c(0,1.05),c(0,1.05),type='n',xaxs='i',yaxs='i',ann=FALSE,xaxt='n',xaxt='n')
