##########################################################################################
## Configure																			##
##########################################################################################

plot_VDwerf2008_config <- function() {
	source("libs/return_multiple_from_functions.r")
	source("libs/dev.off.all.r")
	source("libs/plot_raster_from_vector.r")
	source("libs/plot_3way.r")
	source("libs/loading_glm_and_clin_outputs.r")
	source("libs/plotting_intermediates.r")
	source("libs/load_lpx_results.r")
	source("libs/lm.p_value.r")
	
	source("libs/install_and_source_library.r")
	install_and_source_library("spatstat")
	install_and_source_library("raster")
	
	dev.off.all()
	
	
	preip_data_file	<<- 'data/low_res_rain_aus.nc'
	precip_mrange	<<- c((1994-1970)*12+1,492)
	precip_mms_to_mm<<- FALSE
	
	mod_colname		<<- c("GFED4",	"LPX",		"LPXM-v1-rs"  )
	data_colname	<<- c("GFED4",	"LPX",   	"LPXM-v1-rs")
	obs_not_lpx		<<- c(TRUE,			FALSE,	FALSE		)
	
	filenames		<<- c(	'data/Fire_GFEDv4_Burnt_fraction_0.5grid9.nc',
							'data/lpxr184/benchmarking_runoff_mfire_6762dd_r184_Aus-10156.nc',
							'data/lpxmv1rs/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus-10146.nc')
	varnames		<<- c('mfire_frac','mfire_frac','mfire_frac')
	startp			<<- c(1,10145,10145)
	endp			<<-	c(121+48,10156,10156)
	
	
	lon_range 		<<- c(110,155)
	lat_range		<<- c(-45,-10)
		
	cols			<<- rev(c("FF","DF","BF","9F","6F","3F"))	
	limits			<<- c(0.33,0.5,0.67)
	
	smooth_image	<<- TRUE
	smooth_factor	<<- 5
	
	filename_out		<<- "figs/VDW-limitation"
}

##########################################################################################
## Gubbins																				##
##########################################################################################

plot_VDwerf2008 <- function() {
	
	plot_VDwerf2008_config()

	nmods=length(mod_colname)
	
	png(paste(filenme_out,".png",sep=""),
		height=5*nmods,width=7.5,units = "in",res=600)
	par(mfcol=c(nmods,1),mar=c(0,0,0,0),oma=c(1,1,2,1),cex=1)
	sapply(1:nmods,cal_and_plot_limitations)
	
	dev.off()
}

cal_and_plot_limitations <- function(i) {
	precip=load_nclim_data(preip_data_file,'pr',
		startp=precip_mrange[1],endp=precip_mrange[2])
		
		
	c(xy,mod_NPP,mod_MI,mod_lightn):=open_or_make_model_results(i,precip)
	
	png(paste(filename_out,"-",mod_colname[i],"-",".png",sep=""),
			height=6*1.33,width=7.5*1.33,units = "in",res=600)	
		par(mfcol=c(1,1),mar=c(0,0,2,0),oma=c(1,1,1,1))	

		plot_mans_3way(xy[,1],xy[,2],mod_lightn,mod_NPP,mod_MI,cols,limits)
	
	dev.off()
	
	add.legend=FALSE
	if (i==3) add.legend=TRUE
	
	plot_mans_3way(xy[,1],xy[,2],mod_lightn,mod_NPP,mod_MI,cols,limits,
		lab=FALSE,add_legend=add.legend)
	mtext(data_colname[i],adj=0,cex=2)
}



open_or_make_model_results <- function(i,precip) {
	if (obs_not_lpx[i]) {
		fire				:=load_nclim_data(filenames[i],varnames[i],
										  startp=startp[i],endp=endp[i])
										  
	} else {
		fire				:=load_seasonal_lpx_results(filenames[i],varnames[i],
														startp[i],endp[i],precip)
	}

	c(mod_NPP,mod_MI)	:=make_FAI_reults(fire,precip)
	mod_lightn			=1-mod_NPP-mod_MI
	
	xy=xyFromCell(mod_NPP,1:length(mod_NPP))
	mod_NPP				=1.5*values(mod_NPP)
	mod_MI				=1.5*values(mod_MI)
	mod_lightn			=0.33*values(mod_lightn)
	
	return(list(xy,mod_NPP,mod_MI,mod_lightn))
}



load_seasonal_lpx_results <- function(filename,varname,startp,endp,eg_grid) {

	rout=eg_grid[[1]]
	
	open_ind_yr <- function(i,filename) {
		substr(filename,nchar(filename)-nchar(i)-2,nchar(filename)-3)= as.character(i)
		return(brick(filename,varname=varname))
	}
	
	#rout=sapply(startp:endp,open_ind_yr,filename)
	
	for (i in startp:endp) {
		r=resample(open_ind_yr(i,filename),eg_grid[[1]])
		rout=addLayer(rout,r)
	}
	
	rout=dropLayer(rout,1)
	
	return(rout)
}



make_FAI_reults <- function(obs,precip) {
	
	nyrs=floor(nlayers(obs)/12)
	
	obs=resample(obs,precip)	
	mod_NPP=mod_MI=obs[[1]]
	
	obs=values(obs)
	test_cells=!is.na(obs[,1])
	obs=obs[test_cells,]
	
	precip=values(precip)[test_cells,]
	if (precip_mms_to_mm) 60*60*24*30*precip
	
	mi=c(1,12,36)
	ncells=dim(obs)[1]
	FAI=FDP=afire=matrix(0,ncells,nyrs)
	for (y in 1:nyrs) {
		print(paste("index for year:",y,"off:",nyrs))
		
		c(FAI[,y],FDP[,y]):=find_Is(obs[,mi[1]:mi[2]],precip[,mi[1]:mi[3]])
		afire[,y]=rowSums(obs[,mi[1]:mi[2]])
		mi=mi+12
	}
	
	print("lming FAI")
	NPP=(sapply(1:ncells,lm_R2,FAI,afire))
	print("lming FDP")
	MI=(sapply(1:ncells,lm_R2,FDP,afire))
	
	test=which((NPP+MI)>1)
	tot=NPP[test]+MI[test]
	
	#NPP[test]=NPP[test]/tot
	#MI[test]=MI[test]/tot

	values(mod_NPP)[test_cells]=NPP
	values(mod_MI)[test_cells]=MI
	
	return(list(mod_NPP,mod_MI))
	
}

lm_R2 <- function(i,x,y) {
	if (sum(is.na(x[i,]+y[i,]))>0) {
		return(NaN)
	} else {
		#RS=lm(y[i,]~x[i,])
		#if (is.na(RS$coefficients[2])) {
		#	return(NaN)
		#} else {
	#		return(max(0,1-lm.p_value(RS)))
	#	}
		return(sqrt(summary(lm(y[i,]~x[i,]))$r.squared))
	}
}



find_Is <- function(obs,precip) {
	PFM=rep(NaN,dim(obs)[1])
	test=!is.na(obs[,1])
	PFM[test]=apply(obs[test,],1,which.max)

	FAI=sapply(1:length(PFM),FAI,precip,PFM)
	FDP=sapply(1:length(PFM),FDP,precip,PFM)
	
	return(list(FAI,FDP))	
}

FAI <-function(i,precip,PFM) {
	if (is.na(PFM[i])) {
		return(NaN)
	} else {
		return(sum(precip[i,PFM[i]:(PFM[i]+11)]))
	}
}

FDP <-function(i,precip,PFM) {
	if (is.na(PFM[i])) {
		return(NaN)
	} else {
		m=PFM[i]+12
		precip=precip[i,(m-3):(m+4)]
		test=precip<100
		DM=sum(test,na.rm=TRUE)
		PPT=sum(precip[test],na.rm=TRUE)/DM

		return(sqrt((DM/8)*(1-(PPT/100))))
	}
}





PFM <- function(pdata) {


}
