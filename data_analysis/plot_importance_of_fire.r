## Plot importance of fire:
## (a) World burnt area map
## (b) contribution of IAV fire to CO2
## (c) tree & grass cover vs fire.

configure_part_a <- function() {
	source("libs/install_and_source_library.r")
	source("libs/plot_raster_from_raster.r")
	source("libs/make_col_vector.r")
	source("libs/dev.off.all.r")
	install_and_source_library("raster")
	install_and_source_library("rgdal")
	install_and_source_library("shapefiles")
	
	filename_fire		<<- 	'data/Fire_GFEDv4_Burnt_fraction_0.5grid-nc3.nc'
	filename_fire		<<- 	'data/GFED3_biomassBurning_CO2_1997-2010_20319.nc'
	varname_fire		<<- 	'mfire_frac'
	varname_fire		<<- 	'GFED'
	
	fire_scale			<<- 	60*60*24*365/12
	
	xlims				<<- 	c(-180,180)
	ylims				<<-		c(-60,90)
	
	limits				<<- 	c(0,0.01,0.02,0.05,0.1,.2,0.5,1)*100
	cols				<<- 	c("white","#EE6600","#AA0000","#220000")
	
	conts				<<-		c('Africa','Asia','Australia','North America','Oceania',
								  'South America','Antarctica','Europe')

}

configure_part_c <- function() {
	
	
	filename_fire		<<- 	'data/Fire_GFEDv4_Burnt_fraction_0.5grid9.nc'
	varname_fire		<<- 	'mfire_frac'

	filename_veg.cover	<<-	 	'data/lpxr184/lit_review_control_and_lightn_67b62dd-10156.nc'
	varname_veg.cover	<<- 	'fpc_grid'
}

plot_part_a <- function() {
	configure_part_a()
	dev.off.all()
	png("figs/importancec.png",height=5,width=5,unit='in',res=400)
	
	
	continent=readOGR("data/continent/","continent")
	r=raster(extent(-180,180,-90,90))
	res(r)=c(0.5,0.5)
	continent=rasterize(continent,raster(r))
	
	fire=open_to_annual_average(filename_fire,varname_fire,
		average=TRUE,monthly=TRUE,to_xyz=FALSE)*fire_scale
	
	
	plot_raster_from_raster(fire*100,xlims,ylims,limits=limits,cols=cols,add_legend=FALSE,
							quick=TRUE,regions='.',projection='mollweide')
	add_raster_legend2(make_col_vector(cols,ncols=length(limits)+1),limits,
					   main_title="",xpd=TRUE)
	dev.off.all()
	fire=fire*area(fire)
	cont_fire <- function(cID) {
		continent=continent==cID
		afire=fire*continent
		Carea=sum(values(continent*area(continent)),na.rm=TRUE)
		return(sum(values(afire),na.rm=TRUE)/Carea)
	}
	
	print(paste(conts,sapply(1:length(conts),cont_fire)))
}


plot_part_c <- function() {
	configure_part_c()
	
	fire=open_to_annual_average(filename_fire,varname_fire,
		average=TRUE,monthly=TRUE,to_xyz=FALSE)
		
	tree=open_to_annual_average(filename_veg.cover,varname_veg.cover,
		average=FALSE,monthly=FALSE,to_xyz=FALSE,endp=7)
		
	tree=open_to_annual_average(filename_veg.cover,varname_veg.cover,
		average=FALSE,monthly=FALSE,to_xyz=FALSE,startp=8,endp=9)
		
		browser()
}
	
	
open_to_annual_average <- function(filename,varname,
	average=TRUE,monthly=FALSE,to_xyz=FALSE,startp=1,endp=NULL) {
	source("libs/install_and_source_library.r")
	install_and_source_library("raster")
	
	dati=brick(filename,varname=varname)
	
	if (is.null(endp)) endp=nlayers(dati)
	
	dat=dati[[startp]]

	for (i in (startp+1):endp) {
		dat=dat+dati[[i]]
	}
	
	if (average) {
		dat=dat/nlayers(dati)
		if (monthly) dat=dat*12
	}
	
	if (to_xyz)  {
		val=values(dat)
		dat=cbind(xyFromCell(dat,1:length(val)),val)
	}
	
	return(dat)
	
}
## Part (c)
