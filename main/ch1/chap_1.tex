\begin{savequote}[10cm] % this sets the width of the quote
\sffamily
\includegraphics[height=70mm]{ch1/quote2.png}
%``This is not Nam. This is bowling. There are rules!'' 
%\qauthor{Walter Sobchak - The Big Lebowski}
\end{savequote}

\renewcommand{\chapterheadstartvskip}{\vspace*{4\baselineskip}}
\chapter{A comprehensive benchmarking system for evaluating global vegetation models}

\graphicspath{{ch1/}} 
\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}
\textbf{ D. I. Kelley \textsuperscript{1}, I. C. Prentice, \textsuperscript{1,2}, S. P. Harrison \textsuperscript{1,3}, H.Wang \textsuperscript{1,4}, M. Simard  \textsuperscript{5}, J. B. Fisher \textsuperscript{5}, and K. O. Willis\textsuperscript{1}}
 
 
 \textsuperscript{1}Biological Sciences, Macquarie University, North Ryde, NSW 2109, Australia
 
 \textsuperscript{2}Grantham Institute for Climate Change, and Department of
 Life Sciences, Imperial College, Silwood Park Campus, Ascot SL5 7PY,
 UK
 
 \textsuperscript{3}Geography {\&} Environmental Sciences, School of Human
 and Environmental Sciences, Reading University, Whiteknights, Reading, RG6
 6AB, UK
 
 \textsuperscript{4}State Key Laboratory of Vegetation and Environmental
 Change, Institute of Botany, Chinese Academy of Science, Xiangshan Nanxincun
 20, 100093 Beijing, China
 
 \textsuperscript{5}JPL, California Institute of
 Technology, Pasadena, CA 91109, USA
 
 \begin{center}
 	\color{mygray}\line(1,0){200}
 \end{center}
 \begin{center}	
\textbf{This chapter is presented as the published journal article:}

Kelley, D. I., Prentice, I. C., Harrison, S. P., Wang, H., Simard, M., Fisher, J. B. and Willis, K. O.: A comprehensive benchmarking system for evaluating global vegetation models, Biogeosciences, 10(5), doi:10.5194/bg-10-3313-2013, 2013.

\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}
Reproduced under Creative Commons Attribution 3.0 License courtesy of Copernicus Publications on behalf of the European Geosciences Union (EGU).
%It's likely that you'll need another chapter, so here's a filler just to see
%where it will go \cite{sample}. Also here is a figure \ref{lambda}, so
%you can see how the \emph{hyperref} package works. Note the
%hyperlinking of the citation and figure reference. This will only work if you have the \emph{foronline} option included for the MQThesis document class, at the top of thesis.tex.		

\end{center}

\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}

%\includepdf[pages={1-28}]{bg-10-3313-2013.pdf}


\includepdf[pages=1-28,
	addtotoc={
     		1,section,1,Introduction,pB1,
		2,section,1,Materials and methods,pB2,
     		2,subsection,2,Principles,pB3,
				3, subsection,2,Benchmark datasets,pB4,
				3, subsection,3,{\indent fAPAR},pB5,
				4, subsection,3,{\indent Vegetation cover},pB6,
				5, subsection,3,{\indent NPP},pB7,
				5, subsection,3,{\indent GPP},pB8,
				6, subsection,3,{\indent Canopy height},pB9,
				6, subsection,3,{\indent Burnt fraction},pB10,
				6, subsection,3,{\indent River discharge},pB11,
				6, subsection,3,{\indent CO\subscript{2} concentration},pB12,
     		6,subsection,2,Metrics,pB2,
				8, subsection,3,{\indent Annual average},pB5,
				8, subsection,3,{\indent Inter-annual variability},pB5,
				8, subsection,3,{\indent Seasonality},pB5,
				8, subsection,3,{\indent Relative abundance},pB5,
				9, subsection,3,{\indent Null models},pB5,
			9,subsection,2,Models,pB5,
				9, subsection,3,{\indent Simple Diagnostic Biosphere Model},pB5,
				9, subsection,3,{\indent LPJ},pB5,
				10,subsection,3,{\indent LPX},pB5,
		10,subsection,2,Model protocol,pB5,
	11,section,1,Results,pB5,
		11,subsection,2,Benchmark results,pB5,
			11,subsection,3,{\indent fAPAR},pB5,
			11,subsection,3,{\indent Vegetation cover},pB5,
			11,subsection,3,{\indent NPP/GPP},pB5,
			11,subsection,3,{\indent Canopy height},pB5,
			11,subsection,3,{\indent Burnt fraction},pB5,
			13,subsection,3,{\indent River discharge},pB5,
			13,subsection,3,{\indent CO\subscript{2} concentration},pB5,
		16,subsection,2,Sensitivity tests,pB5,
			16,subsection,3,{\indent Incorporating data uncertainties},pB5,
			18,subsection,3,{\indent The influence of choice of dataset},pB5,
			18,subsection,3,{\indent Benchmarking the sensitivity to parameter tuning},pB5,
	19,section,1,Discussion and conclusion,pB5,
	24,section,1,References,pB5},
	addtolist={
%		4,table,Summary description of the benchmark datasets.,tableBench1,
		4,table,Summary description of the benchmark datasets.,tableBench1,
%		5,figure,{Illustration of the benchmark datasets: ISLSCP II continuous vegetation fields based on a snapshot for 1992--1993 (DeFries and Hansen, 2009) give the proportions of \textbf{(a)} woody vegetation\,$>$\,5\,m in height (tree), \textbf{(b)} grass/herb and woody vegetation\,$<$\,5\,m (herbaceous), and \textbf{(c)} bare ground; for areas with tree cover, the datasets also give the proportion of \textbf{(d)} evergreen, \textbf{(e)} deciduous, \textbf{(f)} broadleaf and \textbf{(g)} needleleaf; \textbf{(i)} annual average fAPAR value for 1998--2005 from SeaWiFS (Gobron \textit{et al.}, 2006); \textbf{(j)} annual average burnt fraction for 1997--2006 from the GFED3 dataset (Giglio \textit{et al.}, 2010); \textbf{(k)} sites with measurements of net primary production, NPP and \textbf{(l)} measurements of gross primary production, GPP are both from the Luyssaert \textit{et al.}~(2007) dataset; \textbf{(m)} global atmospheric CO$_{2}$ concentrations for 1980--2005 based on inversion datasets (Bousquet \textit{et al.}, 2000; R\"{o}denbeck \textit{et al.}, 2003; Baker \textit{et al.}, 2006; Chevalier \textit{et al.}, 2010); \textbf{(n)} annual average river runoff from 1950--2005 from the Dai \textit{et al.}~(2009) dataset, displayed over associated GRDC basins (\url{http://www.bafg.de/GRDC}); and \textbf{(m)} vegetation height based on a snapshot from 2005 (Simard \textit{et al.}, 2011). Hashed area in \textbf{(g)} shows areas without comparison data.},figBench1,
		5,figure,{Illustration of all the benchmark datasets used},figBench1,
%		7,table,{Summary description of the benchmark metrics. $y_{i}$ is the modelled and $x_{i}$ is the corresponding observed value in cell or site $i$, and $\bar{x}$ is the mean observed value across all grid cells or sites. $\omega_{i}$ is the modelled phase, and $\varphi_{i}$ is the observed phase. $q_{ij}$ is the modelled and $p_{i}$ observed proportion of item $j$ in cell or site $i$.},tableBench2,
		7,table,{Summary description of the benchmark metrics},tableBench2,
%		7,table,{Mean, absolute variance (as defined in Eq. 3) and standard deviation (SD) of the annual average values of observations. The variance for most variables is from the long-term mean of the gridded or site data, whereas CO2 is the variance of the inter-annual differences.},tableBench3,
		7,table,{Mean, absolute variance and standard deviation of the annual average values of benchmark observations.},tableBench3,
%		9,figure,{Results of bootstrap resampling of inter-annual variability in global burnt fraction (1997--2005) from the GFED3 dataset. The asterisks labelled LPX and LPJ show the scores achieved by the LPX and LPJ models respectively. The limits for better than and worse than random resampling are set at two standard deviations away from the mean bootstrapping value (vertical lines).},figBench2,
		9,figure,{Results of bootstrap resampling of inter-annual variability in global burnt fraction from observations compared to benchmark scores for LPJ- and LPX-DGVMs.},figBench2,
%		10,figure,{Observed seasonal cycle of atmospheric CO$_{2}$ concentrations at 26 CO$_{2}$ stations over the period 1998--2005 (black line), taken from the CDIAC website (cdiac.ornl.gov) compared to the simulated seasonal cycle from the Simple Diagnostic Biosphere Model (SDBM) (green line); LPJ (red); and LPX (blue). The y-axis indicates variation in atmospheric CO$_{2}$ concentration about the mean. The x-axis is months from January through 18 months to June.},figBench3,
		10,figure,{Observed seasonal cycle of atmospheric CO$_{2}$ concentrations at 26 CO$_{2}$ stations compared to the simulated seasonal cycle from the Simple Diagnostic Biosphere Model; LPJ; and LPX.},figBench3,
%		12,table,{Scores obtained using the mean of the data (Data mean), and the mean and standard deviation of the scores obtained from bootstrapping experiments (Bootstrap mean, Bootstrap SD). NME/NMSE denotes the normalised mean error/normalised mean squared error, MPD the mean phase difference and MM/SCD the Manhattan metric/squared chord distance metrics.},tableBench4,
		12,table,{Scores obtained using the mean of the benchmark data, and the mean and standard deviation of the scores obtained from bootstrapping experiments.},tableBench4,
%		14,table,{Comparison metric scores for model simulations against observations. Mean and variance rows show mean and variance of simulation for annual average values, followed in brackets by the ratio of the mean/variance with observed mean or variance in Table 2.3. Numbers in bold indicate the model with the best performance for that variable. Italic indicates model scores better than the mean of the data score listed in Table 2.4. Asterisks indicate model scores that are significantly better than randomly resampling listed in Table 2.4. NME/NMSE denotes the normalised mean error/normalised mean squared error, MPD the mean phase difference and MM/SCD the Manhattan metric/squared chord distance metrics. fAPAR is the fraction of absorbed photosynthetically active radiation, NPP net primary productivity, and GPP gross primary productivity.},tableBench5,
		14,table,{Mean and variance of simulated annual averages and comparison metric scores for model simulations against observations},tableBench5,
%		17,figure,{Comparison of observed and simulated seasonal phase and seasonal concentration of fraction of absorbed photosynthetically active radiation (fAPAR) averaged over the period 1998--2005 from \textbf{(a)} seasonal phase from SeaWiFS (Gobron \textit{et al.}, 2006) and as simulated by \textbf{(b)} LPJ and \textbf{(c)} LPX; seasonal concentration from \textbf{(d)} SeaWiFS, \textbf{(e)} LPJ and \textbf{(f)} LPX. Hashed area in \textbf{(a)} and \textbf{(d)} shows areas where no comparison is possible.},figBench4,
		17,figure,{Comparison of observed and simulated seasonal phase and seasonal concentration of fraction of absorbed photosynthetically active radiation (fAPAR).},figBench4,
%		18,figure,{Comparisons of observed and simulated NPP and GPP in kg\,C\,m$^{-2}$. The NPP observations (x-axis) are from the dataset made by combining sites from the Luyssaert \textit{et al.}~(2007) dataset and the Ecosystem/Model Data Intercomparison dataset (Olson \textit{et al.}, 2001). The GPP observations are derived from the Luyssaert \textit{et al.}~(2007) dataset. The simulated values (y-axis) are annual averages for the period 1998--2005. The observations are compared with NPP \textbf{(a)} and GPP \textbf{(b)} from the Simple Diagnostic Biosphere Model (SDBM), NPP \textbf{(c)} and GPP \textbf{(d)} from LPJ and NPP \textbf{(e)} and GPP \textbf{(f)} from LPX. The solid line shows the 1\,:\,1 relationship.},figBench5,
		18,figure,{Comparisons of observed and simulated NPP and GPP.},figBench5,
%		18,table,{Mean annual gross primary production (GPP) normalised mean error (NME) comparison metrics using Luyssaert \textit{et al.} (2007) and Beer \textit{et al.} (2010) as alternative benchmarks. In the case of Beer \textit{et al.} (2010), the comparisons are made for all grid cells (global) and also from the grid cells which contain sites in the Luyssaert \textit{et al.} (2007) dataset (at sites).},tableBench6,
		18,table,{Mean annual GPP normalised mean error comparison metrics using site-based and gridded data as alternative benchmarks.},tableBench6,
%		19,figure,{Comparisons of observed and simulated height. \textbf{(a)} Observed canopy height (in 2005) from the Simard \textit{et al.}~(2011) dataset compared to \textbf{(b)} simulated height in the same year from LPX; \textbf{(c)} LPX-simulated height, multiplied by a factor of 2.67 so that the simulated global mean height is the same as the observations; \textbf{(d)} height from \textbf{(c)} with values reduced by a factor of 1.40 about the mean so that the simulations have the same global mean and variance as the observations.},figBench6,
		19,figure,{Comparisons of observed and: LPX simulated height; LPX simulated height scaled so that the simulated global mean height is the same as the observations; and so that the simulated height has the same global mean and variance as the observed.},figBench6,
%		20,table,{Comparison metric scores for simulations with the Simple Diagnostic Biosphere Model (SDBM) against observations of the seasonal cycle of atmospheric CO$_{2}$ concentration and annual average NPP. Numbers in bold indicate the model with the best performance for that variable. \textit{Italic} indicates model scores better than the SDBM simulation tuned using CO$_{2}$ seasonal observations. NME/NMSE denotes the normalised mean error/normalised mean squared error and MPD the mean phase difference. The details of each experiment are explained in the text.},tableBench7,
		20,table,{Comparison metric scores for simulations with the Simple Diagnostic Biosphere Model against observations of the seasonal cycle of atmospheric CO$_{2}$ concentration and annual average NPP},tableBench7,
%		21,figure,{Annual average burnt fraction between 1997--2005 from \textbf{(a)} GFED3 observations (Giglio \textit{et al.}, 2010) and as simulated by \textbf{(b)} LPJ and \textbf{(c)} LPX.},figBench7,
		21,figure,{Annual average burnt fraction from observations and as simulated by LPJ and LPX.},figBench7,
%		21,figure,{Observed inter-annual runoff for 1950--2005 averaged over basins from the Dai \textit{et al.}~(2009) dataset (black line) compared to average simulated runoff over the same basins from LPJ (red line) and LPX (blue line). \textbf{(a)} shows inter-annual runoff, and \textbf{(b)} shows inter-annual variability in runoff where the simulated values are lagged by a year.},figBench8,
		21,figure,{Observed inter-annual runoff compared to runoff simulated by LPJ and LPX showing with and without a one year lag.},figBench8,
%		22,figure,{Comparison of observed and simulated annual average net primary production (NPP). Observed values are from the Luyssaert \textit{et al.}~(2007) and Ecosystem/Model Data Intercomparison datasets (Olson \textit{et al.}, 2001), and the simulated values are from \textbf{(b)} Simple Diagnostic Biosphere Model (SDBM), \textbf{(c)} LPJ and \textbf{(d)} LPX. The symbols in \textbf{(b)}, \textbf{(c)} and \textbf{(d)} indicate the magnitude and direction of disagreement between simulation and observed values, where the upward and downward facing triangles represent over- and undersimulation respectively. Double triangles indicate a difference in NPP of\,$>$\,400\,g\,C\,m$^{-2}$, single filled triangles a difference between 200 and 400\,g\,C\,m$^{-2}$, single empty triangles a difference 100 and 200\,g\,C\,m$^{-2}$, and empty circles a difference of\,$<$\,100\,g\,C\,m$^{-2}$},figBench9,
		22,figure,{Comparison of observed and  simulated annual average net primary production from  Simple Diagnostic Biosphere Model,  LPJ and  LPX},figBench9,
%		23,figure,{Twelve-month running mean of inter-annual variability in global atmospheric CO$_{2}$ concentration between 1998--2005 from Bousquet et al.~(2000), R\"{o}denbeck \textit{et al.}~(2003), Baker \textit{et al.}~(2006) and Chevalier \textit{et al.}~(2010) compared to simulated inter-annual variability from LPJ and LPX.},figBench10
		23,figure,{Comparison of observed inter-annual variability in global atmospheric CO$_{2}$ concentration compared to inter-annual variability simulated by LPJ and LPX.},figBench10
		},pagecommand={\pagestyle{fancy}},scale=0.9,offset=8mm 0mm]{bg-10-3313-2013.pdf}  

%\begin{figure}[tbp]  
%\begin{center}
%\setlength{\unitlength}{1cm}
%\includegraphics[width=8.4\unitlength]{lambda.png}
%\end{center}
%\caption{The $\Lambda$-type three-level energy scheme. States
%$|1\rangle$ and $|2\rangle$ are coupled by the pump pulse $P$ which has a
%detuning $\Delta_P$ from being exactly on resonance.  Similarly states
%$|2\rangle$ and $|3\rangle$ are coupled by the Stokes pulse $S$ which has a
%detuning $ \Delta_S$. State $|2\rangle$ is short lived with spontaneous emission
%occurring out of the system}
%\label{lambda}
%\end{figure}

