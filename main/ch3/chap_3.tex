\begin{savequote}[10cm] % this sets the width of the quote
\sffamily
\includegraphics[height=70mm]{ch3/quote.png}
%``This is not Nam. This is bowling. There are rules!'' 
%\qauthor{Walter Sobchak - The Big Lebowski}
\end{savequote}
\renewcommand{\chapterheadstartvskip}{\vspace*{2\baselineskip}}
\chapter{Improved simulation of fire-vegetation interactions in the Land surface Processes and eXchanges dynamic global vegetation model (LPX-Mv1)}
\graphicspath{{ch1/}} 

\renewcommand{\chapterheadstartvskip}{\vspace*{0.05\baselineskip}}
\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}
\textbf{ D. I. Kelley \textsuperscript{1}, S. P. Harrison \textsuperscript{1,2}, I. C. Prentice, \textsuperscript{1,3}}
 
 \textsuperscript{1}Biological Sciences, Macquarie University, North Ryde, NSW 2109, Australia
 
 \textsuperscript{2}Geography \& Environmental Sciences, School of Archaeology, Geography \& Environmental Sciences, University of Reading, Whiteknights, Reading,
 RG6 6AB, UK.

 \textsuperscript{3}AXA Chair of Biosphere \& Climate Impacts, Grantham Institute for Climate Change \& Dept of Life Sciences, Imperial College, Silwood Park, Ascot, SL5 7PY, UK
 \begin{center}
 	\color{mygray}\line(1,0){200}
 \end{center}
 \begin{center}	
	\textbf{This chapter is presented as the published journal article:}\linebreak
		Kelley, D. I., Harrison, S. P. and Prentice, I. C.: Improved simulation of fire-vegetation interactions in the Land surface Processes and eXchanges Dynamic Global Vegetation Model (LPX-Mv1), Geoscientific Model Development. 7, 2411-2433, 2014
\linebreak
	\textbf{Paper and reviewers discussion available at:}
		www.geosci-model-dev.net/7/2411/2014/
\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}
Reproduced under Creative Commons Attribution 3.0 License courtesy of Copernicus Publications on behalf of the European Geosciences Union (EGU).
%\end{center}

%\begin{center}
	\color{mygray}\line(1,0){200}
\end{center}


\includepdf[pages={1-23},
	addtotoc={
		1,section,1,Abstract,pD0,
     		1,section,1,Introduction,pD1,
		2,section,1,LPX model description,pD2,
     		4,section,1,{Changes to the LPX-M fire module},pD3,
     		4,subsection,2,Lightning ignitions,pD4,
     		6,subsection,2,Fuel drying,pD4,
     		6,subsection,2,Fuel decomposition,pD6,
     		7,subsection,2,Rooting depth,pD7,
     		7,subsection,2,Bark thickness,pD8,
     		9,subsection,2,Resprouting,pD9,
     		11,section,1,Model configuration and test,pD10,
		12,subsection,2,Testing the formulation of resprouting,pD10,
     		14,section,1,Model performance,pD11,
     		15,subsection,2,{LPX-Mv1-nr},pD12,
     		17,subsection,2,{LPX-Mv1-rs},pD13,
     		18,section,1,Discussion,pD14,
     		19,section,1,Conclusions,pD15,
     		19,section,1,References,pD16},
	addtolist={
		3,figure,{Description of the structure of the fire component of LPX.},LPX_f1,
		5,figure,{Observed relationships between: the total and cloud-to-ground lightning flashes; the percentage of dry lightning with respect to the number of wet days per month; and the percentage of dry days with lightning with respect to monthly dry lightning strikes.},LPX_f2,
		7,table,{Plant functional type --- specific values used in LPX-Mv1},tableLPX1,
		8,table,{Translation between LPX Plant Functional Types (PFTs) and the vegetation trait information available for sites which were used to provide rooting depths.},tableLPX2,
		8,figure,{Proportion of roots in the upper 50 cm of the soil by Plant Functional Type.},LPX_f3,
		9,figure,{Bark thickness vs. diameter at breast height for each Plant Functional Type in LPX-M-v1-nr and LPX-M-v1-rs.},LPX_f4,
		10,figure,{Illustration of the variable bark thickness scheme.},LPX_f5,
		12,table,{Summary description of the benchmark datasets.},tableLPX4,
		13,table,{Scores obtained using the mean of the data and the mean and standard deviation of the scores obtained from bootstrapping experiments for steps: 1 - straight comparison; 2 - comparison with the influence of the mean removed; 3 - with mean and variance removed},tableLPX6,
		14,table,{Scores obtained for the individual parameterization experiments, and for  LPX-Mv1-nr and LPX-Mv1-rs compared to the scores obtained for LPX.},tableLPX7,
		15,figure,{Comparison of the observed and simulated abundance of grass, tree and resprouting trees along the climatic gradient in moisture.},LPX_f6,
		15,figure,{Simulated time to recovery after fire compared to observed time to recovery for above-ground resprouters (RS); a combination of other RS types and obligate seeders; and vegetation which does not display specific fire adaptations.},LPX_f7,
		16,figure,{Comparison of observed tree cover and tree cover simulated by LPX-M, LPX-Mv1-nr and LPX-Mv1-rs.},LPX_f8,
		17,figure,{Comparison of annual average burnt area from various remote sensed and ground observations, and as simulated by LPX, LPX-Mv1-nr, LPX-Mv1-rs.},LPX_f9,
		17,figure,{The difference in simulated tree cover and burnt area between the non-resprouting (LPX- MV1-nr) and resprouting (LPX-Mv1-rs) versions of LPX.},LPX_f10},
pagecommand={\pagestyle{fancy}}, scale=0.9,offset=8mm -10mm,clip,trim=0mm 8mm 0mm 2mm]
	{ch3/gmd-7-2411-2014.pdf}



\begin{landscape}

\setcounter{figure}{0}
\renewcommand{\thefigure}{\arabic{chapter}.S\arabic{figure}}
\setcounter{table}{0}
\renewcommand{\thetable}{\arabic{chapter}.S\arabic{table}}
\setcounter{section}{0}
\renewcommand{\thesection}{\arabic{chapter}.S\arabic{section}}

%\includepdf[pages={1-9},
%addtotoc={
%	1,section,1,Supplementary Information,pDs1,
%	3,subsection,2,Additional data information,pDs1a},
%addtolist={
%	3,table,{Allocation of species to plant functional type and to aerial resprouting, non-resprouting, and other resprouting/unknown resprouting categories for the bark thickness analyses.},tableLPX3},
%pagecommand={\pagestyle{fancy}},scale=0.87,offset=8mm 0mm]
%{ch3/gmd-7-2411-2014-supplement.pdf}

%\includepdf[pages={10-14},landscape,
%addtolist={
%	10,table,{Information used to calculate recovery time at sites with different fire-response adaptations as shown in Fig. 4.7.},tableLPX5},
%pagecommand={\pagestyle{fancy}},scale=0.87,offset=0mm -6mm]
%{ch3/gmd-7-2411-2014-supplement.pdf}

%\includepdf[pages={15-31},landscape,
%addtotoc={
%	18,subsection,2,Extended results,pDs1b,
%	25,subsection,2,Wet dat lighting sensitivity analysis,pDs1c,
%	27,subsection,2,Derivation of parameter for grass in Eq. 36,pDs2,
%	28,subsection,2,References for data used to parameterize adaptive bark thickness vs diameter at breast height.,pDs3,
%	30,subsection,2,References,pDs4},
%addtolist={
%	15,table,{Extended version of Table 4.5.},figLPXS3,
%	18,table,{Mean, variance of annual average and comparison metric scores for individual and full model simulations against observations.},figLPXS4,
%	23,figure,{Comparison of annual average burnt area from various remote sensed and ground observations, and as simulated by individual parametrizations of LPX-Mv1.},figLPXS1,
%	24,figure,{Comparison of the simulated abundance of resprouting tree PFTs and their non- resprouting equivalent PFTs along the climatic gradient in moisture.},figLPXS2,
%	25,figure,{Simulated annual average, seasonal and inter-annual burnt area for the wet day lightning sensitivity analysis using LPX-Mv1-rs.},figLPXS2,
%	26,table,{Comparison metric scores for LPX-Mv1-rs and LPX-Mv1-rs incorporating wet day lighting against burnt area observations taken from GFED version 4.},figLPXS4},
%pagecommand={\pagestyle{fancy}},scale=0.87,offset=8mm 0mm]
%{ch3/gmd-7-2411-2014-supplement.pdf}	


\includepdf[pages={1-8},
	addtotoc={
     		  1,section,1,Supplementary Information,pDs1,
		 	  2,subsection,2,Additional data information,pDs1a},
	addtolist={
			  3,table,{Allocation of species to plant functional type and to aerial resprouting, non-resprouting, and other resprouting/unknown resprouting categories for the bark thickness analyses.},tableLPX3,
			  9,table,{Information used to calculate recovery time at sites with different fire-response adaptations as shown in Fig. 4.7.},tableLPX4},
	pagecommand={\pagestyle{fancy}},scale=0.87,offset=8mm 0mm]
	{ch3/Supp_info1.pdf}



\includepdf[pages={1-5},
	addtolist={
		1,table,{Information used to calculate recovery time at sites with different fire-response adaptations as shown in Fig. 4.7.},tableLPX5},
pagecommand={\pagestyle{fancy}},scale=0.87,offset=0mm -6mm]
	{ch3/Supp_info2.pdf}
	


	
\includepdf[pages={1-17},
		addtotoc={
				  1,subsection,2,Extended results,pDs1b,
				  11,subsection,2,Wet dat lighting sensitivity analysis,pDs1c,
	     		  13,subsection,2,Derivation of parameter for grass in Eq. 36,pDs2,
	     		  14,subsection,2,References for data used to parameterize adaptive bark thickness vs diameter at breast height.,pDs3,
	     		  16,subsection,2,References,pDs4},
		addtolist={
				  1,table,{Extended version of Table 4.5.},figLPXS3,
				  4,table,{Mean, variance of annual average and comparison metric scores for individual and full model simulations against observations.},figLPXS4,
				  9,figure,{Comparison of annual average burnt area from various remote sensed and ground observations, and as simulated by individual parametrizations of LPX-Mv1.},figLPXS1,
				  10,figure,{Comparison of the simulated abundance of resprouting tree PFTs and their non- resprouting equivalent PFTs along the climatic gradient in moisture.},figLPXS2,
				  11,figure,{Simulated annual average, seasonal and inter-annual burnt area for the wet day lightning sensitivity analysis using LPX-Mv1-rs.},figLPXS2,
			      12,table,{Comparison metric scores for LPX-Mv1-rs and LPX-Mv1-rs incorporating wet day lighting against burnt area observations taken from GFED version 4.},figLPXS4},
	pagecommand={\pagestyle{fancy}},scale=0.87,offset=8mm 0mm]
		{ch3/Supp_info3.pdf}
	\renewcommand{\thefigure}{\arabic{chapter}.\arabic{figure}}
	\renewcommand{\thetable}{\arabic{chapter}.\arabic{table}}
	\renewcommand{\thesection}{\arabic{chapter}.\arabic{section}}	
\end{landscape}
